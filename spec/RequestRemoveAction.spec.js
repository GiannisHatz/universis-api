import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
const executeInTransaction = TestUtils.executeInTransaction;
describe('RequestRemoveAction', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        return done();
    });
    afterAll( done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        // re-enable development environment
        process.env.NODE_ENV = 'development';
        return done();
    });
    afterEach((done) => {
        delete context.user;
        return done();
    });

    it('should get action', async () => {
        await executeInTransaction(context, async ()=> {
            // get an active student
            let student = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .and('user').notEqual(null)
                .silent()
                .expand('user', 'department')
                .getTypedItem();
            expect(student).toBeTruthy();
            context.locale = 'el';
            // add action
            let newAction = {
                student: student,
                removalYear: student.department.currentYear,
                removalPeriod: student.department.currentPeriod
            };
            await context.model('RequestRemoveAction').silent().save(newAction);
            newAction = await context.model('RequestRemoveAction')
                .where('id').equal(newAction.id)
                .silent()
                .expand('student')
                .getItem();
            expect(newAction).toBeTruthy();
            expect(newAction.inLanguage).toBe('el');
            expect(newAction.student.id).toBe(student.id);
            // get student status
            const isActive = await student.isActive();
            expect(isActive).toBeTruthy();
        });
    });
    it('should add action as student', async () => {
        await executeInTransaction(context, async ()=> {
            // get an active student
            let student = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .and('user').notEqual(null)
                .silent()
                .expand('user', 'department')
                .getTypedItem();
            expect(student).toBeTruthy();
            // set context user (student)
            context.user = {
                name: student.user.name
            };
            // add action
            let newAction = {
                student: student,
                removalYear: student.department.currentYear,
                removalPeriod: student.department.currentPeriod,
                actionStatus: {
                    alternateName: 'ActiveActionStatus'
                }
            };
            await context.model('RequestRemoveAction').save(newAction);
            newAction = await context.model('RequestRemoveAction')
                .where('id').equal(newAction.id)
                .expand('student')
                .getItem();
            expect(newAction).toBeTruthy();
            expect(newAction.student.id).toBe(student.id);
            // get student status
            const isActive = await student.isActive();
            expect(isActive).toBeTruthy();
            // throw error
            // get another student
            let otherStudent = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .and('id').notEqual(student.id)
                .silent()
                .getItem();
            // try to add action for another student
            newAction = {
                student: otherStudent,
                removalYear: student.department.currentYear,
                removalPeriod: student.department.currentPeriod,
                actionStatus: {
                    alternateName: 'ActiveActionStatus'
                }
            };
            await expectAsync((function () {
                return context.model('RequestRemoveAction').save(newAction);
            })()).toBeRejectedWithError('Access Denied');
            // try to add action with invalid status
            newAction = {
                student: student,
                removalYear: student.department.currentYear,
                removalPeriod: student.department.currentPeriod,
                actionStatus: {
                    alternateName: 'CompletedActionStatus'
                }
            };
            await expectAsync((function () {
                return context.model('RequestRemoveAction').save(newAction);
            })()).toBeRejectedWithError('Access Denied');
        });
    });

    it('should get action as student', async () => {
        await executeInTransaction(context, async ()=> {
            // get an active student
            let student = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .and('user').notEqual(null)
                .silent()
                .expand('user', 'department')
                .getTypedItem();
            expect(student).toBeTruthy();
            // set context user (student)
            context.user = {
                name: student.user.name
            };
            // add action
            let newAction = {
                student: student,
                removalYear: student.department.currentYear,
                removalPeriod: student.department.currentPeriod,
                actionStatus: {
                    alternateName: 'ActiveActionStatus'
                }
            };
            await context.model('RequestRemoveAction').save(newAction);
            newAction = await context.model('RequestRemoveAction')
                .where('id').equal(newAction.id)
                .expand('student')
                .getItem();
            expect(newAction).toBeTruthy();
            expect(newAction.student.id).toBe(student.id);

            // get another student
            let otherStudent = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .and('id').notEqual(student.id)
                .silent()
                .getItem();
            newAction = {
                student: otherStudent,
                removalYear: student.department.currentYear,
                removalPeriod: student.department.currentPeriod,
                actionStatus: {
                    alternateName: 'ActiveActionStatus'
                }
            };
            // save silently
            newAction = await context.model('RequestRemoveAction').silent().save(newAction);
            // try to get item
            newAction = await context.model('RequestRemoveAction')
                .where('id').equal(newAction.id)
                .getItem();
            expect(newAction).toBeFalsy();

        });
    });

    it('should claim action', async () => {
        await executeInTransaction(context, async ()=> {
            // get an active student
            let student = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .and('user').notEqual(null)
                .silent()
                .expand('user', 'department')
                .getTypedItem();
            expect(student).toBeTruthy();
            context.locale = 'el';
            context.user = {
                name: student.user.name
            };
            // add action
            let newAction = {
                student: student,
                removalYear: student.department.currentYear,
                removalPeriod: student.department.currentPeriod
            };
            await context.model('RequestRemoveAction').save(newAction);
            // get a user which belongs to Registrar group
            const user = await context.model('User')
                .where('groups/name').equal('Registrar')
                .silent().getItem();
            expect(user).toBeTruthy();
            context.user = {
                name: user.name
            };
            /**
             * @type {RequestRemoveAction}
             */
            newAction = await context.model('RequestRemoveAction').where('id').equal(newAction.id)
                .getTypedItem();
            await newAction.claim();
            let userAction = await context.model('StudentRemoveAction')
                .where('initiator').equal(newAction.id)
                .expand('owner', 'initiator')
                .getItem();
            expect(userAction).toBeTruthy();
            expect(userAction.initiator.id).toBe(newAction.id);
            expect(userAction.owner.name).toBe(context.user.name);
            // cancel action
            userAction.actionStatus = {
                alternateName: 'CancelledActionStatus'
            };
            await context.model('StudentRemoveAction').save(userAction);
            newAction = await context.model('RequestRemoveAction')
                .where('id').equal(newAction.id)
                .getTypedItem();
            expect(newAction).toBeTruthy();
            expect(newAction.actionStatus.alternateName).toBe('CancelledActionStatus');
        });
    });

});
