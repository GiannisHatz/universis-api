import app from '../../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {DocumentNumberService, DocumentNumberVerifierService} from "@universis/docnumbers";
import {EncryptionStrategy} from "@themost/web";
import path from 'path';
import {parse} from 'url';
import request from 'supertest'
import {TestUtils} from '../../server/utils';
import {DataQueryable} from "@themost/data";
const executeInTransaction = TestUtils.executeInTransaction;

describe('DocumentNumberVerifierService', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        // set service
        context = app1.createContext();
        // disable sql logging
        process.env.NODE_ENV = 'test';
        return done();
    });
    afterAll( done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        // re-enable development env
        process.env.NODE_ENV = 'development';
        return done();
    });

    it('should use document verifier service', async () => {
        context.application.getConfiguration().setSourceAt('settings/universis/docnumbers/origin', null);
        expect(() => {
            const service = new DocumentNumberVerifierService(context.application);
            expect(service).toBeFalsy();
        }).toThrowError('Invalid configuration. universis/docnumbers#origin configuration setting is required by DocumentNumberValidator.');

        context.application.getConfiguration().setSourceAt('settings/universis/docnumbers/origin', 'http://example.com');
        const service = new DocumentNumberVerifierService(context.application);
        expect(service).toBeTruthy();
    });

    it('should get document verifier URI', async () => {
        context.application.getConfiguration().setSourceAt('settings/universis/docnumbers/origin', 'http://example.com');
        const service = new DocumentNumberVerifierService(context.application);
        expect(service).toBeTruthy();
        // create document item
        const url = service.generateURI(10, '2020/1/1280');
        // generate locally
        // noinspection ES6MissingAwait
        const toUrl = 'http://example.com/documents/verify?code=' + context.getApplication().getStrategy(EncryptionStrategy).encrypt(JSON.stringify({
            documentSeries: 10,
            documentNumber: '2020/1/1280'
        }));
        expect(url).toBe(toUrl);
    });

    it('should get document verifier code', async () => {

        context.application.getConfiguration().setSourceAt('settings/universis/docnumbers/origin', 'http://example.com');
        const verifierService = new DocumentNumberVerifierService(context.application);

        await executeInTransaction(context,async () => {
            // get department
            const department = await context.model('LocalDepartment').asQueryable().silent().getItem();
            let documentSeries = {
                department: department,
                name: `${department.name} Master`,
                lastIndex: 10,
                numberFormat: '2019/{00000}'
            }
            await context.model('DepartmentDocumentNumberSeries').silent().save(documentSeries);
            documentSeries = await context.model('DepartmentDocumentNumberSeries')
                .where('id').equal(documentSeries.id)
                .silent()
                .getTypedItem();
            // get user
            const user = await context.model('User').where('groups/name').equal('Administrators').silent().getItem();
            expect(user).toBeTruthy();
            context.user = user;
            /**
             * @type {DocumentNumberService}
             */
            const service = context.application.getService(DocumentNumberService);
            // get formatted document number
            const documentNumber = await documentSeries.nextAndFormat();
            const file = path.resolve(__dirname, '../files/LoremIpsum.pdf');
            // noinspection JSCheckFunctionSignatures
            let newItem = await service.add(context, file, {
                name: 'LoremIspum.pdf',
                contentType: 'application/pdf',
                parentDocumentSeries: documentSeries,
                documentNumber: documentNumber
            });
            const item = await context.model('DocumentNumberSeriesItem')
                .where('id').equal(newItem.id)
                .silent()
                .getItem();

            const url = verifierService.generateURI(item.parentDocumentSeries, item.documentNumber);
            expect(url).toBeTruthy();

            // noinspection JSCheckFunctionSignatures
            let response = await request(app)
                .get('/documents/verify')
            expect(response.status).toBe(400);



            response = await request(app)
                .get('/documents/verify?code=test')
            expect(response.status).toBe(400);

            const query = parse(url).query;

            // get original DataQueryable.getItem
            const getItem = DataQueryable.prototype.getItem;
            // spy on DataQueryable.getItem
            spyOn(DataQueryable.prototype, 'getItem').and.callFake(
                /**
                 * @this DataQueryable
                 * @returns {Promise<unknown>|*}
                 */
                function() {
                    // eslint-disable-next-line no-invalid-this
                    const thisArg = this;
                    // if model is DocumentNumberSeriesItem
                    if (thisArg.model.name === 'DocumentNumberSeriesItem') {
                        return Promise.resolve(item);
                    } else {
                        // otherwise pass through
                        return getItem.bind(thisArg)();
                    }
            });
            response = await request(app)
                .get(`/documents/verify?${query}`)
            expect(response.status).toBe(200);

        });


    });



});
