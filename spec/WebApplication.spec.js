import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';

const executeInTransaction = TestUtils.executeInTransaction;
describe('WebApplication', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        return done();
    });
    afterAll(done => {
        if (context) {
            return context.finalize(() => done());
        }
        // re-enable development environment
        process.env.NODE_ENV = 'development';
        return done();
    });
    afterEach((done) => {
        delete context.user;
        return done();
    });

    it('should create WebApplication', async () => {

        const newApplication = {
            name: 'Universis Students',
            alternateName: 'students',
            applicationSuite: 'universis',
            applicationCategory: 'public',
            softwareVersion: 'latest'
        };
        await context.model('WebApplication').silent().save(newApplication);
        const item = await context.model('WebApplication')
            .where('alternateName').equal('universis-students').silent().getItem();
        expect(item).toBeTruthy();
        expect(item.name).toBe('Universis Students');
        item.name = 'Universis Students Latest Version';
        await context.model('WebApplication').silent().save(item);
        const count = await context.model('WebApplication')
            .where('alternateName').equal('universis-students').silent().count();
        expect(count).toBe(1);

        await executeInTransaction(context, async () => {
            const newApplication = {
                name: 'Universis Students',
                alternateName: 'universis-students',
                applicationSuite: 'Universis',
                applicationCategory: 'Web application',
                softwareVersion: 'latest'
            };
            await context.model('WebApplication').silent().save(newApplication);
            const item = await context.model('WebApplication')
                .where('alternateName').equal('universis-students').silent().getItem();
            expect(item).toBeTruthy();
            expect(item.name).toBe('Universis Students');
            item.name = 'Universis Students Latest Version';
            await context.model('WebApplication').silent().save(item);
            const count = await context.model('WebApplication')
                .where('alternateName').equal('universis-students').silent().count();
            expect(count).toBe(1);
        });
    });

});