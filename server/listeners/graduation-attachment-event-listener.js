/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */

import _ from 'lodash';
import {Guid} from "@themost/common/utils";
import {DataConflictError} from "../errors";



export function beforeSave(event, callback) {
    return GraduationAttachmentEventListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return callback();
}

class GraduationAttachmentEventListener {

    static async beforeSaveAsync(event) {
        const target = event.model.convert(event.target);
        /**
         * @type {DataContext|*}
         */
        const context = event.model.context;
        if (event.state !== 4) {
            // check if type of supplied object is guid
            if (event.target.object) {
                if (!Guid.isGuid(event.target.object)) {
                    // get graduation event
                    const graduationEvent = _.isObject(event.target.object) ? event.target.object.id : event.target.object;
                    //load identifier from graduation event
                    event.target.object = await context.model('GraduationEvent').select('identifier').where('id').equal(graduationEvent).value();
                }
            } else {
                // graduation event should be supplied
                throw new DataConflictError('E_EVENT_MISSING', context.__('Object graduation event is missing'),  'GraduationAttachmentConfiguration');
            }
        }

    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {

    }
}
