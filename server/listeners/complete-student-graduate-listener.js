/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
import {DataError} from "@themost/common";
import ActionStatusType from '../models/action-status-type-model';
import Student from '../models/student-model';
import * as _ from 'lodash';

export function beforeSave(event, callback) {
    (async () => {
        // validate student state (on insert or update)
        if (event.state === 1 || event.state === 2) {
            let context = event.model.context;
            /**
             * @type {Student|DataObject|*}
             */
            const studentStatus = await context.model('Student').silent().where('id').equal(event.target.object)
                .silent()
                .select('studentStatus')
                .value();
            if (studentStatus == null) {
                throw new DataError('Action object cannot be found or is inaccessible');
            }
            // if student is not declared, throw error
            if (studentStatus.alternateName !== 'declared') {
                throw new DataError('ERR_STATUS',
                    'Invalid student status. Student graduation action may execute upon a declared student only.'
                    , null,
                    'Student', 'studentStatus');
            }
        }
    })().then( () => {
       return callback();
    }).catch( err => {
        return callback(err);
    });
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    (async () => {
        const context = event.model.context;
        let target;
        // get own properties
        const attributes = event.model.attributes.filter( attribute => {
            if (attribute.primary) {
                return false;
            }
            return attribute.model === event.model.name;
        }).map( attribute => {
            return attribute.name;
        });
        // on insert
        if (event.state === 1) {
            // get target object
            target = await context.model(event.model.name).where('id').equal(event.target.id).silent().getTypedItem();
            // validate completed state
            if (target.actionStatus.alternateName !== ActionStatusType.CompletedActionStatus) {
                return;
            }

        }
        // on update
        else if (event.state === 2) {
            // get previous state
            const previousActionStatus = event.previous.actionStatus;
            // get target object
            target = await context.model(event.model.name).where('id').equal(event.target.id).silent().getTypedItem();
            if ((previousActionStatus.alternateName === ActionStatusType.ActiveActionStatus &&
                target.actionStatus.alternateName === ActionStatusType.CompletedActionStatus) === false) {
                return;
            }
        }
        else {
            return;
        }
        // do student graduation (patch student)
        // pick student graduation attributes
        const student = _.pick(target, attributes);
        // assign id and studentStatus
        Object.assign(student, {
            id: target.object.id,
            studentStatus: {
                alternateName: 'graduated'
            }
        });
        // get graduationEvent from initiator
        const request = await context.model('GraduationRequestAction')
            .where('id').equal(target.initiator).flatten().getItem();
        if (request && request.graduationEvent)
        { // assign  graduation event
            Object.assign(student, {
                graduationEvent:
                    {
                        "id": request.graduationEvent
                    }
            });
        }
        // do update
        await context.model('Student').silent().save(student);
        // remove student declaration
        const studentDeclarations = await context.model('StudentDeclaration').where('student').equal(target.object).silent().getItems();
        // delete student declaration
        if (studentDeclarations.length > 0) {
            //delete studentDeclarations
            await context.model('StudentDeclaration').silent().remove(studentDeclarations);
        }

    })().then( () => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}
