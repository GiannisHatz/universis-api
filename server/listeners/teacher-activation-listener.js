/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    try {
        const context = event.model.context;
        if (event.state===1) {
            const applicationUserData = {
                userId: event.target["id"],
                applicationId: 'universis',
                roleId: 401,
                enabled: -1,
            };
            const applicationUserModel = context.model("ApplicationUser");
            applicationUserModel.save(applicationUserData, function (err) {
                if (err) {
                    return callback(err);
                }
                callback();
            });
        }
        else {
            callback();
        }
    }
    catch (e) {
        callback(e)
    }
}