import StudyProgramSpecialty from '../models/study-program-specialty-model';
import {TranslateService} from "../services/translate-service";
// eslint-disable-next-line no-unused-vars
import {ExpressDataContext} from '@themost/express';
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    // add printName if not supplied
    if (!event.target.printName)
    {
        event.target.printName = event.target.name;
    }
    return callback();
}
/**
 * Add core specialization while inserting a new study program
 * @param {DataEventArgs} event
 */
export async function afterSaveAsync(event) {
    // after insert
    if (event.state === 1) {
        // noinspection JSValidateTypes
        /**
         * get current context
         * @type {ExpressDataContext}
         */
        const context = event.model.context;
        // get translate service
        const translateService = context.application.getStrategy(TranslateService);
        // and try to get default name and abbreviation
        const name = translateService.translate('Specialization.CoreName');
        const abbreviation = translateService.translate('Specialization.CoreAbbreviation');
        // finally insert core specialization
        await context.model("StudyProgramSpecialty").silent().save({
            studyProgram: event.target,
            name: name,
            abbreviation: abbreviation,
            specialty: -1
        });
        // add also calculation rule for all course types
        await context.model("CalculationRule").silent().save({
            studyProgram: event.target,
            ruleType: {
                "alternateName": "CourseType"
            },
            checkValues: -1,
            coefficient: 1
        });
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then( () => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}
