import _ from "lodash";
import moment from "moment";
/**
 * @param {DataEventArgs} event
 * @returns {Promise<void>}
 */
async function beforeSaveAsync(event) {

    const context = event.model.context;
    let calculateIdentifiers = false;
    // check if student status is changed from candidate to active
    if (event.state === 2) {
        // get previous state
        const previousStudentStatus = event.previous.studentStatus;
        if (previousStudentStatus.alternateName === 'candidate' && event.target.studentStatus && event.target.studentStatus.alternateName === 'active') {
            // calculate student identifiers
            calculateIdentifiers = true;
        }
    }

    if (event.state === 1 || calculateIdentifiers) {
        /**
         * @type {DataContext|*}
         */
        if ((_.isNil(event.target.studentInstituteIdentifier) || _.isNil(event.target.studentIdentifier)) || calculateIdentifiers)
        {
        // get department to calculate student identifiers
        const department = await context.model('LocalDepartment').where('id').equal(event.target.department)
            .select('id','studentIdentifierIndex','studentIdentifierFormat','studentInstituteIdentifierFormat','studentInstituteIdentifierIndex').getItem();

        const inscriptionYear = _.isObject(event.target.inscriptionYear)? event.target.inscriptionYear.id : event.target.inscriptionYear;

            if (_.isNil(event.target.studentIdentifier) || calculateIdentifiers) {
                if (department.studentIdentifierFormat) {
                    event.target.studentIdentifier = calculateIdentifier(department.studentIdentifierFormat, department.studentIdentifierIndex, inscriptionYear);
                    department.studentIdentifierIndex = department.studentIdentifierIndex + 1;
                }
            }
            if (_.isNil(event.target.studentInstituteIdentifier) || calculateIdentifiers) {
                if (department.studentIdentifierFormat) {
                    event.target.studentInstituteIdentifier = calculateIdentifier(department.studentInstituteIdentifierFormat, department.studentInstituteIdentifierIndex,inscriptionYear);
                    department.studentInstituteIdentifierIndex = department.studentInstituteIdentifierIndex + 1;
                }
            }
            await context.model('LocalDepartment').silent().save(department);
        }
    }

    // TODO: calculate student current semester and inscription retro year and period
    if (event.state === 1)
    {
        event.target.inscriptionRetroYear = event.target.inscriptionYear;
        event.target.inscriptionRetroPeriod = event.target.inscriptionPeriod;
     }

    // set specialtyId from studyProgramSpecialty
    if (!_.isNil(event.target.studyProgramSpecialty)) {
        const studyProgramSpecialty =  _.isObject(event.target.studyProgramSpecialty)? event.target.studyProgramSpecialty.id : event.target.studyProgramSpecialty;
        event.target.specialtyId= await context.model('StudyProgramSpecialty').where('id').equal(studyProgramSpecialty).select('specialty').value();
    }

}

function calculateIdentifier (studentIdentifierFormat, studentIdentifierIndex, inscriptionYear) {
    let studentIdentifier;
    studentIdentifier = studentIdentifierFormat;
    let format = studentIdentifierFormat.split(';');
    for (let i = 0; i < format.length; i++) {
        const formatElement = format[i];
        if (formatElement.startsWith('I')) {
            // index with leading zeros
            studentIdentifier = studentIdentifier.replace(formatElement, zeroPad((studentIdentifierIndex + 1), formatElement.length - 1));
        }
        if (formatElement.startsWith('T')) {
            // text
            studentIdentifier = studentIdentifier.replace(formatElement, formatElement.substr(1));
        }
        if (formatElement.startsWith('Y')) {
            // inscription year
            let today = new Date();
            today.setFullYear(inscriptionYear, 1, 1, 0);
            studentIdentifier = studentIdentifier.replace(formatElement, moment(today).format(formatElement));
        }
    }
    return studentIdentifier.replace(/;/g, '');
}
function zeroPad(num, places) {
    let zero = places - num.toString().length + 1;

    return Array(+(zero > 0 && zero)).join("0") + num;
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}
