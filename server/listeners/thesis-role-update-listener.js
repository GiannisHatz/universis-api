import { DataError } from "@themost/common";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return ThesisMemberUpdateListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return callback();
}

class ThesisMemberUpdateListener {

    static async beforeSaveAsync(event) {
        // get context
        const context = event.model.context;
        let status;
        if (event.state != 1) {
            return;
        }
        const thesis = context.model('Thesis').convert(event.target.thesis);
        // get status
        status = await context.model('Thesis')
                .where('id').equal(thesis.getId())
                .select('status/alternateName')
                .silent()
                .value();
        // throw error of thesis status is either completed or cancelled
        if (status === 'completed' || status === 'cancelled') {
            throw new DataError('E_THESIS_STATUS', 'Invalid thesis status. Members of a completed or cancelled thesis cannot be modified.', null, 'Thesis');
        }
    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        // if event state is other than insert exit
        if (event.state != 1) {
            return;
        }
        // get context
        const context = event.model.context;
        const thesis = context.model('Thesis').convert(event.target.thesis);
        // get all thesis students
        const students = await context.model('StudentThesis')
            .where('thesis').equal(thesis.getId())
            .silent()
            .take(-1)
            .getItems();
        // if thesis does not have any student    
        if (students.length === 0) {
            // return
            return;
        }
        // otherwise create a student thesis result for each student 
        // which refers to newly added member (instructor)
        const items = students.map( x => {
            return {
                // set student
                student: x.student,
                // set student thesis reference
                studentThesis: x.id,
                // set instructor
                instructor: event.target.member
            }
        });
        await context.model('StudentThesisResult').silent().save(items);
    }
}