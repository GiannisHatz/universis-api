import {DataError, DataNotFoundError} from "@themost/common";
import {DataModel} from "@themost/data";
class CourseClassListener {

    /**
     * @param {DataEventArgs} event
     */
    static async beforeSaveAsync(event) {
        const context = event.model.context;
        const target = event.target;
        // exit on update
        if (event.state !== 1) {
            return;
        }
        // check if course is complex and throw error
        const course = await context.model('Course').where('id').equal(target.course).getItem();
        if (!course) {
            // course does not exist
            throw new DataNotFoundError(context.__('Course not found'));
        }
        if (course.courseStructureType === 4) {
            // creating courseClass for complex course is not allowed
            throw new DataError(context.__('Creating course class for complex course is not allowed'));
        }
    }

    /**
     * @param {DataEventArgs} event
     */
    static async beforeRemoveAsync(event) {
        const context = event.model.context;
        const target = event.target;
        const model=event.model;
        const getReferenceMappings = DataModel.prototype.getReferenceMappings;
        model.getReferenceMappings   = async function () {
            const res = await getReferenceMappings.bind(this)();
            // remove readonly model CourseExamStudentGrade, StudentAvailableClasses from mapping before delete
            const mappings = ['CourseExamStudentGrade','StudentAvailableClass'];
            return res.filter((mapping) => {
                return mappings.indexOf(mapping.childModel)<0;
            });
        };
            //check courseClass connections
            //course class can be deleted only if courseClassStudents don't exist
            const numberOfStudents = await context.model('StudentCourseClass').where('courseClass').equal(target.id).count();
            if (numberOfStudents === 0) {
                const exams = await context.model('CourseExamClass').where('courseClass').equal(target.id).select('id').getItems();
                if (exams && exams.length) {
                    await context.model('CourseExamClass').remove(exams);
                }
            } else {
                //cannot delete courseClass
                throw new DataError('Cannot delete course class');
            }

    }
    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        const context = event.model.context;
        const target = event.target;
        if (event.state === 1) {
            // check if courseClass has sections, instructors and rules and save all
            // save course class instructors
            if (target.instructors && target.instructors.length) {
                target.instructors = target.instructors.map(x => {
                    x.courseClass = target.id;
                    return x;
                });
                await context.model('CourseClassInstructor').save(target.instructors);
            }
            // save course class sections
            if (target.sections && target.sections.length) {
                target.sections = target.sections.map(x => {
                    x.courseClass = target.id;
                    return x;
                });
                await context.model('CourseClassSection').save(target.sections);
            }
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return CourseClassListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return CourseClassListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return CourseClassListener.beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}
