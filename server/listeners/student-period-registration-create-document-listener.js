import {TraceUtils} from '@themost/common/utils';
import util from "util";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    try {
        let self = event.target, context = event.model.context;
        if (event.state === 1 || event.state === 2) {
            // convert target to StudentPeriodRegistration
            self = context.model('StudentPeriodRegistration').convert(self);
            self.studentOf(function (err, student) {
                if (err) {
                    return callback(err);
                }
                self.classes = self.classes || [];
                return context.model('Student').convert(student).is(':me').then(isMe => {
                    //add registration document
                    const inserted = self.classes.filter(function (x) {
                        if (typeof x.validationResult === 'undefined')
                            return false;
                        return x.validationResult.success && x.validationResult.code === 'SUCC';
                    });
                    const deleted = self.classes.filter(function (x) {
                        if (typeof x.validationResult === 'undefined')
                            return false;
                        return x.validationResult.success && x.validationResult.code === 'DEL';
                    });
                    if (deleted.length === 0 && inserted.length === 0 && self.classes.length !== 0) {
                        return callback();
                    }
                    let insClasses = "", delClasses = "";
                    if (inserted.length > 0) {
                        insClasses = util.format('%s:%s', context.__('Inserted courses'), inserted.map(function (x) {
                            return util.format('%s', x.displayCode)
                        }).join(', '));
                    }
                    if (deleted.length > 0) {
                        delClasses = util.format('%s:%s', context.__('Deleted courses'), deleted.map(function (x) {
                            return util.format('%s', x.displayCode)
                        }).join(', '));
                    }
                    const registrationYear = context.model('AcademicYear').convert(self.registrationYear).getId();
                    const registrationPeriod = context.model('AcademicPeriod').convert(self.registrationPeriod).getId();

                    if (isMe === false) {
                        if (self.classes.length===0) {
                            return callback();
                        }
                        // add only EventLog and exit
                        return context.model('Student').where('id').equal(student).select('id', 'studentIdentifier', 'person/familyName as familyName', 'person/givenName as givenName', 'user')
                            .getItem().then((studentInfo) => {
                                const eventTitle = util.format('%s [%s] %s %s %s-%s %s,%s'
                                    ,context.__('Registration changes'), studentInfo.studentIdentifier, studentInfo.familyName, studentInfo.givenName, registrationYear, registrationYear + 1, insClasses, delClasses);
                                return context.model('EventLog').silent().save({
                                    title: eventTitle,
                                    eventType: 3,
                                    username: context.user.name
                                }, function (err) {
                                    return callback(err);
                                });
                            }).catch(err => {
                                return callback(err);
                            });

                    }
                    const query = util.format("sp_createStudentRegistrationDocument %s,%s,%s", registrationYear, registrationPeriod, student);
                    context.db.execute(query, null, function (err, result) {
                        if (err) {
                            TraceUtils.error(err);
                            return callback(err);
                        } else {
                            try {

                                let eventTitle = "";
                                if (self.classes.length === 0) {
                                    eventTitle = util.format("Η δήλωση του φοιτητή με περιγραφή [%s] Αποστολή δήλωσης [%s][%s] ] ακαδ.έτους %s-%s αποθηκεύθηκε. Η κατάστασή της είναι <%s>"
                                        , self.id, result[0]['name'], result[0]['studentIdentifier'], result[0]['registrationYear'], result[0]['registrationYear'] + 1, result[0]['docStatusReason']);
                                } else
                                    eventTitle = util.format('Τροποποιήσεις δήλωσης [%s] %s  %s-%s %s,%s'
                                        , result[0]['studentIdentifier'], result[0]['name'], result[0]['registrationYear'], result[0]['registrationYear'] + 1, insClasses, delClasses);

                                context.unattended(function (cb) {
                                    context.model('EventLog').save({
                                        title: eventTitle,
                                        eventType: 3,
                                        username: context.interactiveUser.name
                                    }, function (err) {
                                        cb(err);
                                    });
                                }, function (err) {
                                    if (err) {
                                        TraceUtils.error(err);
                                    }
                                    callback();
                                });
                            } catch (e) {
                                return callback(e);
                            }
                        }
                    });
                }).catch(err => {
                    return callback(err);
                });

            });
        }
    } catch (e) {
        callback(e);
    }
}
