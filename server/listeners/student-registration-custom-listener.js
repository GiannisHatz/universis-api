import _ from 'lodash';
import {LangUtils} from '@themost/common/utils';

/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
export function beforeSave(event, callback) {
    try {
        callback();
    }
    catch (e) {
        callback(e);
    }
}

/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
export function afterSave(event, callback) {
    try {
        const target = event.target, context = event.model.context;
        if (event.state === 1 || event.state === 2) {
            let classes;
            if (_.isArray(target['classes'])) {
                classes = target['classes'].filter(function (x) {
                    return x.$state !== 4;
                });
            }
            else {
                return callback();
            }

            const filtered = classes.filter(function (x) {
                x.validationResult = x.validationResult || [];
                return x.validationResult.success === true;
            });
            if (filtered.length === 0) {
                return callback();
            }
            const student = event.model.idOf(event.target["student"]);
            context.model('Student').where('id').equal(student).select('id', 'department', 'studyProgram','semester','specialtyId').flatten().silent().first(function (err, result) {
                if (err) {
                    callback(err);
                }
                else {
                    context.model('LocalDepartment').where('id').equal(result['department']).select('checkCustomRules').first(function (err, dep) {
                        if (err) {
                            return callback(err);
                        }
                        try {
                            if (LangUtils.parseInt(dep['checkCustomRules']) === 0) {
                                return callback();
                            }
                            return callback();
                        } catch (e) {
                            return callback(e);
                        }
                    });
                }
            });

        }
        else {
            callback();
        }
    }
    catch (e) {
        callback(e);
    }
}