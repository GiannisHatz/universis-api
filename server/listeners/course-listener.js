import {DataError} from "@themost/common";

class CourseListener {
    /**
     * @param {DataEventArgs} event
     */
    static async beforeSaveAsync(event) {

        const target = event.model.convert(event.target);
        const context = event.model.context;
        const previous =event.previous;

        // get structure type
        const courseStructureType = event.target.courseStructureType;
        if (courseStructureType == null && event.state===2) {
           //load from course
            const structureType = await target.property('courseStructureType').getItem();
            target.courseStructureType = structureType;
        }
        //rule 1 : cannot change courseStructureType
        if (previous && (previous.courseStructureType != target.courseStructureType)) {
            throw new DataError('ERR_INVALID_DATA',
                context.__('Structure type cannot be changed')
                , null,
                'Course');
        }
        // rule 2: remove courseParts from update
        if ( event.state===2 && target.courseParts && target.courseParts.length>0) {
            // remove target.courseParts
            delete event.target.courseParts;
        }
    }

    /**
     * @async
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        const context = event.model.context;
        if (event.state===1) {
            if (event.target.courseParts && event.target.courseParts.length > 0 && event.target.courseStructureType ===4) {

                // check if coursePart exists and throw error
                const courseIds = event.target.courseParts.map(coursePart => {
                    return coursePart.id;
                });
                if (courseIds) {
                    const courses = await context.model('Course').where('id').in(courseIds).getItems();
                    if (courses.length > 0) {
                        throw new DataError('ERR_INVALID_DATA',
                            context.__('Course is not available for coursePart')
                            , null,
                            'Course');
                    }
                }
                let i = 0;
                event.target.courseParts.forEach(course => {
                    i += 1;
                    course.id = course.id ||  `${event.target.id}` + i;
                    course.courseStructureType = 8;
                    course.department = event.target.department;
                    course.gradeScale = event.target.gradeScale;
                    course.parentCourse = event.target.id;
                    course.instructor = course.instructor || event.target.instructor;
                    course.$state = 1;
                });
                await context.model('Course').save(event.target.courseParts);
            }
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return CourseListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return CourseListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}
