import {DataError} from "@themost/common";
import Student from '../models/student-model';

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    (async () => {
        // validate student state (on insert)
        if (event.state === 1) {
            let context = event.model.context;
            /**
             * @type {Student|DataObject|*}
             */
            const student = context.model('Student').convert(event.target.student);
            const isActive = await student.silent().isActive();
            if (!isActive) {
                throw new DataError('ERR_STATUS',
                    'Invalid student status. Student cannot be declared.'
                    , null,
                    'Student', 'studentStatus');
            }
        }
    })().then( () => {
       return callback();
    }).catch( err => {
        return callback(err);
    });
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    (async () => {
        const context = event.model.context;
        // on insert
        if (event.state === 1) {
            const student = await context.model('Student')
                .find(event.target.student)
                .select('id', 'studentStatus')
                .silent()
                .getItem();
            // change student status
            student.studentStatus = {
                alternateName: 'declared'
            };
            // do update
            await context.model('Student').silent().save(student);
        }
        //
        else {
            return;
        }
    })().then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
