import {DataConflictError, ValidationResult} from "../errors";
import {TraceUtils} from "@themost/common";
import {promisify} from "util";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return ValidateRequestDocumentActionListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return callback();
}

class ValidateRequestDocumentActionListener {

    static async beforeSaveAsync(event) {
        // get context
        const context = event.model.context;
        if (event.state != 1) {
            return;
        }
        const student = context.model('Student').convert(event.target.student);

        // get document configuration
        /**
         * @type {DocumentConfiguration}
         */
        let config = await context.model('DocumentConfiguration')
            .find(event.target.object)
            .silent()
            .getTypedItem();

        if (config) {
            // add document configuration description to document request if empty
            event.target.name = event.target.name || config.name;
            event.target.alternateName = event.target.alternateName || config.alternateName;
        }
        else {
            throw new DataConflictError('E_DOCUMENT_TYPE_MISSING', context.__('Document type is missing'), 'RequestDocumentAction');

        }
        // validate document if validationResult is emplty
        if (!config.validationResult) {
            config.student = student;
            const validateAsync = promisify(config.validate)
                .bind(config);
            config.validationResult  = await validateAsync();

        }
        if (config.validationResult && config.validationResult.success===false) {
            throw new DataConflictError('RULE', config.validationResult.message,
                'StudentRequestAction');
        }

        // check maximum active requests for this student
        if (config.maximumActive > 0) {
            const active = await context.model(event.model.name)
                .where('student').equal(student.id)
                .and('actionStatus/alternateName').equal('ActiveActionStatus')
                .and('object').equal(config.id)
                .silent().count();
            if (active >= config.maximumActive) {
                throw new DataConflictError('ERR_ACTIVE_ACTION_MAX_REACHED', context.__('Number of maximum allowed active requests has been reached for this request type'),
                    'StudentRequestAction');
            }
        }
        // check maximum completed requests
        if (config.maximumAllowed > 0) {
            const completed = await context.model(event.model.name)
                .where('student').equal(student.id)
                .and('actionStatus/alternateName').equal('CompletedActionStatus')
                .and('object').equal(config.id)
                .silent().count();

            if (completed >= config.maximumAllowed) {
                throw new DataConflictError('ERR_COMPLETED_ACTION_MAX_REACHED', context.__('Number of maximum allowed completed requests has been reached for this request type'),
                    'StudentRequestAction');
            }
        }
    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        return;
    }
}
