import ActionStatusType from '../models/action-status-type-model';
import {TraceUtils} from "@themost/common";

async function beforeSaveAsync(event) {
    if (event.state !== 1) {
        return;
    }
    const target = event.model.convert(event.target);
    const context = event.model.context;
    // build description
    const student = await context.model('Student').where('id').equal(target.object).select('id',
        'studentIdentifier', 'person/familyName as familyName', 'person/givenName as givenName').getItem();
    if (Array.isArray(target.groups)) {
        // load groups from model
        const groupIds = target.groups.map(function (x) {
            return x.id;
        });
        target.groups = await context.model('ProgramGroup').where('id').in(groupIds).getItems();
        const groups = target.groups.map(x => {
            return x.name;
        }).join(', ');
        event.target.description = `${context.__('Program group selection')}: ${student.studentIdentifier} ${student.familyName} ${student.givenName} (${groups}) `;
    }
}

async function afterSaveAsync(event) {
    if (event.state !== 2) {
        return;
    }
    const context = event.model.context;
    const target = event.model.convert(event.target);
    // get action status
    const actionStatus = await target.property('actionStatus').getItem();
    // if action status is other that active
    if (actionStatus.alternateName !== ActionStatusType.CompletedActionStatus) {
        // exit
        return;
    }
    // get previous action status
    const previousActionStatus = event.previous && event.previous.actionStatus;
    if (previousActionStatus === null) {
        throw new Error('Previous status cannot be empty at this context.');
    }
    // validate previous status
    if (previousActionStatus.alternateName !== 'ActiveActionStatus') {
        // throw error for invalid previous action status
        throw new Error('Invalid action state. An action cannot be completed due to its previous state.');
    }
    // execute action by assigning student and specialty to an instance of StudentSpecialty class
    // get student
    const student = await target.property('object').silent().getTypedItem();
    let cancelAction = false;
    // get available student groups
    if (Array.isArray(target.groups) && target.groups.length>0) {
        // get available student groups
        const availableGroups = await student.availableProgramGroups();
        if (availableGroups.length > 0) {
            // load groups from model
            const groupIds = target.groups.map(function (x) {
                return x.id;
            });
            target.groups = await context.model('ProgramGroup').where('id').in(groupIds).getItems();
            // check if defined groups exist in available groups
            for (let i = 0; i < target.groups.length; i++) {
                const group = context.model('ProgramGroup').convert(target.groups[i]);
                // check if group exist in available program groups
                const exists = availableGroups.find(availableGroup => {
                    return availableGroup.groups.find(x => {
                        return group.id === x.id;
                    });
                });
                if (!exists) {
                    target.description = target.description || '';
                    target.description += context.__('NotAvailableProgramGroup');
                    cancelAction = true;
                }

                // validate program group rules
                const validationResult =await group.validate(student.id);
                if(validationResult && validationResult.finalResult)
                {
                    if (validationResult.finalResult.success ===false) {
                        target.description = target.description || '';
                        target.description += `${group.name}: ${validationResult.finalResult.message}`;
                            cancelAction = true;
                    }
                }
            }
            // check  max allowed groups
            availableGroups.forEach(superGroup => {
                // check groups
                const groups = target.groups.filter(x => {
                    return x.parentGroup === superGroup.id;
                });
                if (groups.length > superGroup.maxAllowedGroups) {
                    // student action should be cancelled
                    target.description = target.description || '';
                    target.description +=  context.__('MaxAllowedProgramGroupsReached');
                    cancelAction = true;
                }
            });
        } else {
            // student action should be cancelled
            target.description = target.description || '';
            target.description += context.__('NotAvailableProgramGroups');
            cancelAction = true;
        }
    } else {
        target.description = target.description || '';
        target.description += context.__('NoGroupsSupplied');
        cancelAction = true;
    }
    if (cancelAction === true) {
        target.actionStatus = {"alternateName": "CancelledActionStatus"};
        await event.model.silent().save(target);
        Object.assign(event.target, {
            actionStatus: target.actionStatus
        });
    } else {
        // save student groups
        let body=context.__('Program group selection');
        const studentGroups = target.groups.map(group => {
            const programGroup = Object.assign({}, group);
            const studentGroup = Object.assign({}, group, {
                student,
                programGroup
            });
            delete studentGroup.id;
            body += `<li class="pt-1">${studentGroup.description || studentGroup.name}</li>`;
            return studentGroup;

        });
        await context.model('StudentProgramGroup').silent().save(studentGroups);

        try {
            // send message to student
            const isMe = await student.is(':me');
            if (isMe) {
                const message = {
                    student: event.target.object,
                    action: event.target.id,
                    subject: context.__('CompletedStudentRequestSubject'),
                    body: body.substr(0, 8000)
                };
                // save student message
                await context.model('StudentMessage').silent().save(message);
            }
        }
       catch (e) {
           TraceUtils.log(e);
       }

    }
 }
export function afterSave(event, callback) {
    if (event.state !== 2) {
        return callback();
    }
    // execute async method
    return afterSaveAsync(event).then((validationResult) => {
        event.target.validationResult = validationResult;
        if (validationResult && !validationResult.success) {
            return callback(validationResult);
        }
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
export function beforeSave(event, callback) {
    // execute async method
    return beforeSaveAsync(event).then((validationResult) => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
