/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
import {TraceUtils} from "@themost/common";
import {ApplicationService} from "@themost/common/app";
import {DataConfigurationStrategy} from '@themost/data';
import path from 'path';

export function beforeSave(event, callback) {
    (async function () {
        // if state is other than insert
        if (event.state !== 1) {
            // do nothing
            return;
        }
        /**
         * @type {DataContext|*}
         */
        const context = event.model.context;
        // get student status
        const studentStatus =  await context.model('Student').silent().where('id').equal(event.target.student)
            .silent()
            .select('studentStatus')
            .value();
        if (studentStatus == null) {
            throw new Error('Action object cannot be found or is inaccessible');
        }
        // if student is active or declared
        if (studentStatus.alternateName === 'active' || studentStatus.alternateName === 'declared') {
            // do nothing
            return;
        }
        // set failed action status
        Object.assign(event.target, {
            actionStatus: {
                alternateName: "FailedActionStatus"
            }
        });
    })().then(() => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    (async function () {
        // if state is other than insert
        if (event.state !== 1) {
            // do nothing
            return;
        }
        // if action has status other than failed
        if (event.target.actionStatus.alternateName !== 'FailedActionStatus') {
            // do nothing
            return;
        }
        /**
         * @type {DataContext|*}
         */
        const context = event.model.context;
        // send auto message to inform student
        await context.model('StudentMessage').silent().save({
            student: event.target.student,
            action: event.target.id,
            subject: context.__('FailedStudentRequestSubject'),
            body: context.__('FailedStudentRequestBody')
        });
    })().then(() => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}

export class AutoRejectStudentRequestAction extends ApplicationService {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        // call super constructor
        super(app);
        // install service
        this.install();
    }

    install() {
        /**
         * get data configuration
         * @type {DataConfigurationStrategy}
         */
        const configuration = this.getApplication().getConfiguration().getStrategy(DataConfigurationStrategy);
        // get StudentRequestAction model definition
        const model = configuration.getModelDefinition('StudentRequestAction');
        // get this file path relative to application execution path
        const listenerType = './' + path.relative(this.getApplication().getConfiguration().getExecutionPath(), __filename);
        // ensure model event listeners
        model.eventListeners = model.eventListeners || [];
        // try to find event listener
        const findIndex = model.eventListeners.findIndex(listener => {
            return listener.type === listenerType ||
                listener.type === listenerType.replace(/\.js$/, '');
        });
        if (findIndex < 0) {
            // add event listener
            model.eventListeners.push({
                type: listenerType
            });
            // update StudentRequestAction model definition
            configuration.setModelDefinition(model);
            // write to log
            TraceUtils.info('Services: AutoRejectStudentRequestAction service has been successfully installed');
        }
    }

    uninstall() {
        /**
         * get data configuration
         * @type {DataConfigurationStrategy}
         */
        const configuration = this.getConfiguration().getStrategy(DataConfigurationStrategy);
        // get StudentRequestAction model definition
        const model = configuration.getModelDefinition('StudentRequestAction');
        // get this file path relative to application execution path
        const listenerType = './' + path.relative(this.getApplication().getConfiguration().getExecutionPath(), __filename);
        // ensure model event listeners
        model.eventListeners = model.eventListeners || [];
        // try to find event listener
        const findIndex = model.eventListeners.find( listener => {
            return listener.type === listenerType ||
                listener.type === listenerType.replace(/\.js$/, '');
        });
        if (findIndex>=0) {
            // remove event listener
            model.eventListeners.splice(findIndex, 1);
            // update StudentRequestAction model definition
            configuration.setModelDefinition(model);
            // write to log
            TraceUtils.info('Services: AutoRejectStudentRequestAction service has been successfully uninstalled');
        }
    }
}
