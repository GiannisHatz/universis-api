import {DataError} from "@themost/common";
import {ValidationResult} from "../errors";
import * as _ from "lodash";

class StudentGradeListener {
    /**
     * @param {DataEventArgs} event
     */
    static async beforeSaveAsync(event) {
        if (event.state === 1) {
            const context = event.model.context;
            const target = event.model.convert(event.target);
            try {
                // find related courseClass
                if (!target.courseClass) {
                    // get latest studentClass
                    const examClasses = await context.model('CourseExamClass')
                        .where('courseExam').equal(target.courseExam)
                        .select('courseClass').getItems();
                    if (examClasses) {
                        const studentCourseClass = await context.model('StudentCourseClass').where('student').equal(target.student)
                            .and('courseClass').in(examClasses.map(x => {
                                return x.courseClass;
                            })).orderByDescending('courseClass/period').getItem();
                        if (!studentCourseClass) {
                            throw new DataError(context.__('Student course registration not found'));
                        }
                        event.target.courseClass = studentCourseClass.courseClass;
                    }
                }
            } catch (err) {
                target.validationResult = new ValidationResult(false, 'INVDATA', err.message);
                throw err;
            }

        }
    }

    /**
     * @async
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {

        const context = event.model.context;
        const previous = event.previous;
        const target = event.model.convert(event.target);

        // get grade status
        const gradeStatus = await target.property('status').getItem();
        // if action status is other that active
        if (gradeStatus.alternateName === 'pending') {
            // exit
            return;
        }
        /**
         * @type {StudentGrade}
         */
        const studentGrade = await context.model('StudentGrade').where('student').equal(target.student)
            .and('courseExam').equal(target.courseExam).expand('courseExam', 'courseClass').getTypedItem();
        if (gradeStatus.alternateName === 'normal') {
            // get previous status
            // get grade status
            const previousStatus = previous ? await context.model('GradeStatus').where('identifier').equal(previous.status).getItem() : null;
            if (event.state === 1 || (event.state === 2 && (previousStatus.alternateName === 'pending' || target.examGrade != previous.examGrade))) {
                // validate grade
                try {
                    const result = await (await studentGrade.validate());
                    // update studentCourse, studentClass with latest grade
                    /**
                     * @type {StudentCourse}
                     */
                    let studentCourse = await context.model('StudentCourse')
                        .where('student').equal(target.student)
                        .and('course').equal(studentGrade.courseExam.course).getTypedItem();
                    if (studentCourse) {
                        //check if grade refers to latest examPeriod
                        let lastGrade = await studentCourse.getLastGrade();
                        if (lastGrade === null) {
                            lastGrade = target;
                        }
                        if (lastGrade.gradeYear < studentGrade.courseExam.year || (lastGrade.gradeYear === studentGrade.courseExam.year
                            && lastGrade.examPeriod <= studentGrade.courseExam.examPeriod)) {
                            studentCourse = _.assign(studentCourse, lastGrade);
                            await context.model('StudentCourse').save(studentCourse);
                        }
                    } else {
                        throw new DataError(context.__('Course not found'));
                    }
                    // update studentClass
                    const studentClass = await context.model('StudentCourseClass').where('student').equal(studentGrade.student)
                        .and('courseClass').equal(studentGrade.courseClass).getItem();
                    if (studentClass) {
                        const isLastGrade = await context.model('StudentGrade').where('student').equal(studentGrade.student)
                            .and('courseClass').equal(studentGrade.courseClass).orderByDescending('courseExam/examPeriod')
                            .and('status/alternateName').equal('normal')
                            .getItem();
                        if (isLastGrade.id === studentGrade.id) {
                            // update studentClass grade
                            studentClass.examGrade = studentGrade.examGrade;
                            studentClass.finalGrade = studentGrade.examGrade;
                            studentClass.examPeriod = studentGrade.courseExam.examPeriod;
                            await context.model('StudentCourseClass').save(studentClass);
                        }
                    } else {
                        throw new DataError(context.__('Student course registration not found'));
                    }
                } catch (err) {
                    target.validationResult = new ValidationResult(false, 'INVDATA', err.message);
                    throw err;
                }
            }
        }
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return StudentGradeListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return StudentGradeListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
