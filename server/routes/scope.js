import {ScopeAccessConfiguration} from "../services/scope-access-configuration";
import {HttpForbiddenError} from "@themost/common";

/**
 * Validates user authentication scopes against requested resource based on current application configuration
 * @returns {Function}
 */
function validateScope() {
    return (req, res, next) => {
        /**
         * @type {ScopeAccessConfiguration}
         */
        let scopeAccessConfiguration = req.context.getApplication().getConfiguration().getStrategy(ScopeAccessConfiguration);
        if (typeof scopeAccessConfiguration === 'undefined') {
            return next(new Error('Invalid application configuration. Scope access configuration strategy is missing or is in accessible.'));
        }
        scopeAccessConfiguration.verify(req).then(value => {
            if (value) {
                return next();
            }
            return next(new HttpForbiddenError('Access denied due to authorization scopes.'))
        }).catch(reason => {
            return next(reason);
        });
    };
}

module.exports.validateScope = validateScope;