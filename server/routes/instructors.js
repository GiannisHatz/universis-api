import express from 'express';
import {getEntityFunction, getEntitySetFunction, postEntityAction} from '@themost/express';
import Instructor from "../models/instructor-model";
import {
    DataError,
    HttpBadRequestError,
    HttpConflictError,
    HttpForbiddenError,
    HttpNotFoundError
} from "@themost/common";
import {multerInstance} from './multer';
import {csvPostParser} from '../middlewares/csv';
import {PrivateContentService} from "../services/content-service";
import path from 'path';
import {ValidationResult} from "../errors";
import {xlsPostParser, XlsxContentType} from "../middlewares/xls";
import {TraceUtils, Args} from '@themost/common';
import {interactiveInstructor} from "../middlewares";
import {getMailer} from "@themost/mailer";
import moment from "moment";
import {promisify} from 'util';
import fs from  'fs';
import tmp from 'tmp';


/**
 * @param {ConfigurationBase} configuration
 * @returns Router
 */
function instructorsRouter(configuration) {
    let router = express.Router();
    // set user storage from application configuration
    const upload = multerInstance(configuration);
    upload.storage.getDestination(null, null, (err, destination) => {
        if (err) {
            TraceUtils.error(`Routes: An error occurred while initializing instructors router.`);
            return TraceUtils.error(err);
        }
        TraceUtils.info(`Routes: Instructors router starts using "${path.resolve(destination)}" as user storage.`);
    });
    function readStream(stream) {
        return new Promise((resolve, reject) => {
            let buffers = [];
            stream.on('data', (d) => {
                buffers.push(d);
            });
            stream.on('end', () => {
                return resolve(Buffer.concat(buffers));
            });
            stream.on('error', (err) => {
                return reject(err);
            });
        });
    }

    /**
     * @returns {RequestHandler}
     */
    function bindInstructor() {
        return (req, res, next) => {
            // get instructor
            Instructor.getMe(req.context).then( getInstructor => {
                return getInstructor.getTypedItem().then ( instructor => {
                    // set instructor to current request params
                    req.params.instructor = instructor;
                    return next();
                });
            }).catch((err) => {
                return next(err);
            });
        };
    }

    /**
     * @swagger
     *
     * /api/Instructors/Me/Classes/Students:
     *  get:
     *    tags:
     *      - Instructor
     *    description: Returns a collection of students which have been registered in one or more instructor's courses
     *    security:
     *      - OAuth2:
     *          - teachers
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              type: object
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    router.get('/me/classes/students', function getClassesStudents (req, res, next) {
        // set entity set parameters
        Object.assign(req.params, {
            entitySetFunction: "me",
            entityFunction : "classStudents"
        });
        // drop $select query parameter
        delete req.query.$select;
        // go to entitySetFunction middleware
        return next();
    }, getEntitySetFunction({
        entitySet: "Instructors",
    }));

    /**
     * @swagger
     *
     * /api/Instructors/Me/Classes/{courseClass}/Students/sendMessage:
     *   post:
     *    tags:
     *      - Instructor
     *    description: Send a message to all students registered for a specific courseClass
     *    security:
     *      - OAuth2:
     *          - teachers
     *    parameters:
     *      - in: path
     *        name: courseClass
     *        description: An integer which represents the id of a course class
     *        schema:
     *          type: integer
     *        required: true
     *    responses:
     *      '200':
     *        description: success
     *      '404':
     *        description: not found
     *      '403':
     *        description: forbidden
     *      '500':
     *        description: internal server error
     */
    router.post('/me/classes/:courseClass/students/sendMessage', upload.single('file'), bindInstructor(), async function sendCourseClassStudentMessages (req, res, next) {
        try {
            let instructor = req.params.instructor;
            if (typeof instructor === 'undefined') {
                return next(new HttpForbiddenError());
            }
            // check if instructor email is set
            if (!instructor.email || (instructor.email && instructor.email.length === 0)) {
                return next(new DataError('Instructor email is not set. Cannot continue'));
            }
            /**
             * get instructor courseClass
             * @type {CourseClass}
             */
            let courseClass = await instructor.getInstructorClasses().where('id').equal(req.params.courseClass).expand('year','period').getTypedItem();
            if (typeof courseClass === 'undefined') {
                return next(new HttpNotFoundError('Course class cannot be found or is inaccessible'));
            }

            // get all students
            const students = await req.context.model('StudentCourseClass')
                .where('courseClass').equal(courseClass.id)
                .take(-1)
                .select('student', 'student/person/email as email', 'student/studentIdentifier as studentIdentifier').getItems();

            // set description to courseClass title
            let model = {
                sender: instructor,
                courseClass: courseClass,
                body: req.body.body,
                subject:req.body.subject,
                category:'InstructorMessage',
                dateCreated: new Date(),
                message: req.context.__('Send message to course class students')
            };

            const mailTemplate = await req.context.model('MailConfiguration').where('target').equal('CourseClassStudentsMessage').select('template').value() || 'instructor-to-students-message';
            await sendMessages( req, students, model,mailTemplate);
            return res.json(null);
        }
        catch (err) {
            return next(err);
        }
    });


    /**
     * @swagger
     *
     * /api/Instructors/Me/exams/{courseExam}/Students/sendMessage:
     *   post:
     *    tags:
     *      - Instructor
     *    description: Send a message to all students of a course exam
     *    security:
     *      - OAuth2:
     *          - teachers
     *    parameters:
     *      - in: path
     *        name: courseExam
     *        description: An integer which represents the id of a course exam
     *        schema:
     *          type: integer
     *        required: true
     *    responses:
     *      '200':
     *        description: success
     *      '404':
     *        description: not found
     *      '403':
     *        description: forbidden
     *      '500':
     *        description: internal server error
     */
    router.post('/me/exams/:courseExam/students/sendMessage', upload.single('file'), bindInstructor(), async function sendCourseExamStudentMessages (req, res, next) {
        try {
            let instructor = req.params.instructor;
            if (typeof instructor === 'undefined') {
                return next(new HttpForbiddenError());
            }
            /**
             * get instructor courseExam
             * @type {CourseExam}
             */
            let courseExam = await instructor.getInstructorExams().where('id').equal(req.params.courseExam).expand('year','examPeriod').getTypedItem();
            if (typeof courseExam === 'undefined') {
                return next(new HttpNotFoundError('Course exam cannot be found or is inaccessible'));
            }
            // check if instructor email is set
            if (!instructor.email || (instructor.email && instructor.email.length === 0)) {
                return next(new DataError('Instructor email is not set. Cannot continue'));
            }
            // get all students
            const students = await ((await courseExam.getStudents()).select('student/studentIdentifier as studentIdentifier','student/person/email as email').silent().getAllItems());
            // set courseExam period from examPeriod for use at template
            courseExam.period= courseExam.examPeriod;
            courseExam.title = courseExam.name;
            // set description to courseExam title
            let model = {
                sender: instructor,
                courseClass: courseExam,
                body: req.body.body,
                subject:req.body.subject,
                category:'InstructorMessage',
                dateCreated: new Date(),
                message: req.context.__('Send message to course exam students')
            };

            const mailTemplate = await req.context.model('MailConfiguration').where('target').equal('CourseClassStudentsMessage').select('template').value() || 'instructor-to-students-message';
            await sendMessages( req, students, model,mailTemplate);
            return res.json(null);
        }
        catch (err) {
            return next(err);
        }
    });

    function sendMessages(req, students, model, template) {
        const app = req.context.getApplication();
        const context = app.createContext();
        context.locale = req.locale;
        let failed = [];
        let succeeded = [];
        (async function () {
            // check if message contains attachment
            let filePath;
            if (req.file) {
                // get temp directory and copy attached file
                const dirAsync = promisify(tmp.dir);
                const tmpDirectory = await dirAsync();
                const copyFileASync = promisify(fs.copyFile);
                let s = path.resolve(process.cwd(), req.file.path);
                let s1 = path.resolve(tmpDirectory, req.file.originalname);
                await copyFileASync(s, s1);
                filePath = s1;
            }
            // send message for each student
            for (let i = 0; i < students.length; i++) {
                let student = students[i];
                try {
                    await new Promise((resolve, reject) => {
                        const mailer = getMailer(context)
                            .replyTo(model.sender.email)
                            .subject(model.subject)
                            .template(template)
                            .to(student.email);
                        // add attachment
                        if (filePath) {
                            mailer.attachments([filePath])
                        }
                        mailer.send(Object.assign(model, {
                            html: {
                                moment: moment
                            }
                        }), (err) => {
                            if (err) {
                                TraceUtils.error(err);
                                // add to failed array if error
                                failed.push(student.email || student.studentIdentifier);
                                return resolve();
                            }
                            succeeded.push(student.email);
                            return resolve();
                        });
                    });

                } catch (err) {
                    TraceUtils.error(err);

                }
            }
        })().then(() => {
            context.finalize(() => {
                // build message body
                let messageBody = `<h6 class='pt-1'>${model.courseClass.title} (${model.courseClass.year.alternateName} - ${model.courseClass.period.name}) </h6>  ${model.body} <div class='pt-2'> ${context.__('Total messages')}: ${students.length} , ${context.__('Successfully sent')}: ${succeeded.length}, ${context.__('Failed')}: ${failed.length}`;
                if (failed.length>0)
                {
                    messageBody += `<div><i>${context.__('MessageFailedInfo')}</i></div>`;
                }

                // after finishing sending mails, an instructor message is created to inform instructor
                const message = Object.assign({}, req.body, {
                    subject: model.message,
                    body: messageBody,
                    instructor: model.sender,
                    category: 'InstructorMessage',
                    about: `${model.courseClass.id}`
                });
                return req.context.model('InstructorMessage').save(message).then(message => {
                    //  send message to instructor
                    TraceUtils.info(`Message from instructor ${model.sender.email} to course class ${model.courseClass.title} Total messages: ${students.length} , Sent: ${succeeded.length}, Failed: ${failed.length}`);
                }).catch(err => {
                    TraceUtils.error(err);
                });
            });
        }).catch(err => {
            context.finalize(() => {
                TraceUtils.error(`An error occurred while sending message to courseClass ${model.courseClass.title}`);
                TraceUtils.error(err);
                // An instructor message is created to inform instructor about the error
                let messageBody = `<h6 class='pt-1'>${model.courseClass.title} (${model.courseClass.year.alternateName} - ${model.courseClass.period.name}) </h6>  ${model.body} <div class='pt-2'> ${context.__('Total messages')}: ${students.length} , ${context.__('Successfully sent')}: ${succeeded.length}, ${context.__('Failed')}: ${failed.length}`;
                if (failed.length>0)
                {
                    messageBody += `<div><i>${context.__('MessageFailedInfo')}</i></div>`;
                }
                const message = Object.assign({}, req.body, {
                    subject: model.message,
                    body: messageBody,
                    instructor: model.sender,
                    category: 'InstructorMessage',
                    about: `${model.courseClass.id}`
                });
                return req.context.model('InstructorMessage').save(message).then(message => {
                    //  send message to instructor
                    TraceUtils.info(`Message from instructor ${model.sender.email} to course class ${model.courseClass.title} Total messages: ${students.length} , Sent: ${succeeded.length}, Failed: ${failed.length}`);
                }).catch(err => {
                    TraceUtils.error(err);
                });
            });
        });
    }

    /**
     * @swagger
     *
     * /api/Instructors/Me/Classes/{courseClass}/Students/:student/sendMessage:
     *   post:
     *    tags:
     *      - Instructor
     *    description: Send a message to student for a specific courseClass
     *    security:
     *      - OAuth2:
     *          - teachers
     *    parameters:
     *      - in: path
     *        name: courseClass
     *        description: An integer which represents the id of a course class
     *        schema:
     *          type: integer
     *        required: true
     *      - in: path
     *        name: student
     *        description: An integer which represents the id of a student
     *        schema:
     *          type: integer
     *        required: true
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/StudentMessage'
     *      '404':
     *        description: not found
     *      '403':
     *        description: forbidden
     *      '500':
     *        description: internal server error
     */
    router.post('/me/classes/:courseClass/students/:student/sendMessage', upload.single('file'), bindInstructor(), async function sendCourseClassStudentMessage (req, res, next) {
        try {
            let instructor = req.params.instructor;
            if (typeof instructor === 'undefined') {
                return next(new HttpForbiddenError());
            }
            /**
             * get instructor courseClass
             * @type {CourseClass}
             */
            let courseClass = await instructor.getInstructorClasses().where('id').equal(req.params.courseClass).getTypedItem();
            if (typeof courseClass === 'undefined') {
                return next(new HttpNotFoundError('Course class cannot be found or is inaccessible'));
            }

            // get student
            const student = await req.context.model('StudentCourseClass')
                .where('courseClass').equal(courseClass.id)
                .and('student').equal(req.params.student)
                .select('Export').getItem();

            if (typeof student === 'undefined') {
                return next(new HttpNotFoundError('Student cannot be found or is inaccessible.'));
            }
            // set description to courseClass title
            req.body.description = courseClass.title;
            req.body.about=`CourseClasses/${courseClass.id}`;
            let result = await sendStudentMessage(req, student.student);
            return res.json(result);
        }
        catch (err) {
            return next(err);
        }
    });


    /**
     * @swagger
     *
     * /api/Instructors/Me/Theses/Students/:student/sendMessage:
     *   post:
     *    tags:
     *      - Instructor
     *    description: Send a message to student for a specific thesis
     *    security:
     *      - OAuth2:
     *          - teachers
     *    parameters:
     *      - in: path
     *        name: student
     *        description: An integer which represents the id of a student
     *        schema:
     *          type: integer
     *        required: true
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/StudentMessage'
     *      '404':
     *        description: not found
     *      '403':
     *        description: forbidden
     *      '500':
     *        description: internal server error
     */
    router.post('/me/theses/:thesis/students/:student/sendMessage',upload.single('file'), bindInstructor(), async function sendThesisStudentMessage (req, res, next) {
        try {
            let instructor = req.params.instructor;
            if (typeof instructor === 'undefined') {
                return next(new HttpForbiddenError());
            }
            /**
             * get instructor theses
             * @type {CourseClass}
             */
            let studentThesis = await instructor.getInstructorThesisStudents()
                .where('student').equal(req.params.student)
                .and('thesis').equal(req.params.thesis)
                .expand('thesis').getTypedItem();
            if (typeof studentThesis === 'undefined') {
                return next(new HttpNotFoundError('Student thesis cannot be found or is inaccessible'));
            }
            // set description to courseClass title
            req.body.description = studentThesis.thesis.name;
            req.body.about=`Theses/${studentThesis.thesis.id}`;
            let result = await sendStudentMessage(req, studentThesis.student);
            return res.json(result);
        }
        catch (err) {
            return next(err);
        }
    });

    async function sendStudentMessage(request, student) {
        return new Promise((resolve, reject) => {
            let finalResult;
            return request.context.db.executeInTransaction((cb) => {
                let message=Object.assign({ }, request.body, {
                    student: student,
                    category: 'InstructorMessage'
                });
                return request.context.model('StudentMessage').save(message).then(message => {
                    // convert message to studentMessage
                    finalResult = request.context.model('StudentMessage').convert(message);
                    if (request.file) {
                        // add attachment to message
                        return request.context.unattended( unattendedCallback => {
                            return finalResult.addAttachment(request.file).then(result => {
                                finalResult.attachments=[];
                                finalResult.attachments.push(result);
                                return unattendedCallback();
                            }).catch( err => {
                                return unattendedCallback(err);
                            });
                        }, err=> {
                            return cb(err);
                        });
                    }
                    return cb();
                }).catch(err => {
                    return cb(err);
                });
            }, (err)=> {
                if (err) {
                    return reject(err);
                }
                return resolve(finalResult);
            });
        });
    }


    /**
     * @swagger
     *
     * /api/Instructors/Me/Classes/{courseClass}/Students/Export:
     *  get:
     *    tags:
     *      - Instructor
     *    description: Exports a collection of students who have been registered to the given course class
     *    parameters:
     *      - in: path
     *        name: courseClass
     *        description: A variant which represents the id of a course class
     *        schema:
     *          type: string
     *        required: true
     *    security:
     *      - OAuth2:
     *          - teachers
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/csv:
     *            schema:
     *              type: array
     *              items:
     *                  type: object
     *          application/vnd.openxmlformats-officedocument.spreadsheetml.sheet:
     *            schema:
     *              type: array
     *              items:
     *                  type: object
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    router.get('/me/classes/:courseClass/students/export', bindInstructor(), async function exportCourseClassStudents (req, res, next) {
        try {
            /**
             * get instructor object
             * @type {Instructor}
             */
            let instructor = req.params.instructor;
            // check if instructor is undefined
            if (typeof instructor === 'undefined') {
                // and throw error
                return next(new HttpForbiddenError());
            }
            /**
             * get instructor course class
             * @type {CourseClass}
             */
            let courseClass = await instructor.getInstructorClasses().where('id').equal(req.params.courseClass).select('id').getTypedItem();
            // check if course exam is undefined
            if (typeof courseClass === 'undefined') {
                // and throw error
                return next(new HttpNotFoundError('Instructor course class cannot be found'));
            }
            // apply custom filter
            // get only $filter and $orderby query params
            let getStudents = await req.context.model('StudentCourseClass').filter({
                $filter: req.query.$filter,
                $orderby: req.query.$orderby
            });
            // set course class filter
            getStudents.prepare().and('courseClass').equal(courseClass.id);
            /**
             * get export view
             * @type {DataModelView}
             */
            let view = getStudents.model.getDataView('Export');
            // get student data
            let students = await getStudents.select('Export').getAllItems();
            // validate content type
            if (req.accepts(XlsxContentType)) {
                // return xlsx
                return res.xls(view.toLocaleArray(students)).then(() => res.end());
            }
            // return csv
            return res.csv(view.toLocaleArray(students)).then(() => res.end());
        }
        catch (err) {
            return next(err);
        }
    });

    /**
     * @description A request handler for setting course class student view for instructors.
     * This operation blocks instructors from getting more data than they are permitted to view.
     */
    router.get('/me/classes/:courseClass/students/?$',  async function setCourseClassStudentParams(req, res, next) {
        // set $select query option to export
        req.query.$select="InstructorClassStudents";
        // return
        return next();
    });


    router.get('/me/classes/:courseClass/:courseClassFunction', function getCourseClassFunction (req, res, next) {
        // get instructor course class
        Instructor.getMe(req.context).then(q => {
            // get instructor object
            return q.select('id').getTypedItem().then(
                /**
                 * @param {Instructor} instructor
                 */
                instructor =>  {
                    // check if instructor is undefined
                    if (typeof instructor === 'undefined') {
                        // and continue
                        return next(new HttpNotFoundError());
                    }
                    return instructor.getInstructorClasses().where('id').equal(req.params.courseClass).select('id').getTypedItem().then(
                        /**
                         * @param {CourseClass} courseClass
                         */
                        courseClass=> {
                            // check if course class is undefined
                            if (typeof courseClass === 'undefined') {
                                // and continue
                                return next(new HttpNotFoundError());
                            }
                            // call middleware
                            return getEntityFunction({
                                entityFunctionFrom: 'courseClassFunction',
                                from: 'courseClass',
                                entitySet: 'CourseClasses'
                            })(req, res, next);
                        });
                }).catch(reason => {
                return next(reason);
            });
        });
    });
    /**
     * @description A request handler for setting course exam student view for instructors.
     * This operation blocks instructors from getting more data than they are permitted to view.
     */
    router.get('/me/exams/:courseExam/students/?$',  async function getCourseExamStudents(req, res, next) {
        // set $select query option for instructor

        if (req.query.$group == null && req.query.$groupby == null)
        {
            req.query.$select="CourseExamInstructorView";
        }
        // return
        return next();
    });

    /**
     * @description A request handler for setting course exam student view for instructors.
     * This operation blocks instructors from getting more data than they are permitted to view.
     */
    router.get('/me/exams/:courseExam/participants/?$',  async function getCourseExamParticipants(req, res, next) {
        // set $select query option for instructor

        if (req.query.$group == null && req.query.$groupby == null)
        {
            req.query.$select="CourseExamInstructorView";
        }
        // return
        return next();
    });

    /**
     * @description A request handler for setting course exam student view for instructors.
     * This operation blocks instructors from getting more data than they are permitted to view.
     */
    router.post('/me/exams/:courseExam/types/?$', bindInstructor(), async function setTestTypes(req, res, next) {
        /**
         * get instructor object
         * @type {Instructor}
         */
        let instructor = req.params.instructor;
        // check if instructor is undefined
        if (typeof instructor === 'undefined') {
            // and throw error
            return next(new HttpForbiddenError());
        }
        let courseExam = await instructor.getInstructorExams().where('id').equal(req.params.courseExam).select('id').getTypedItem();
        // check if course exam is undefined
        if (typeof courseExam === 'undefined') {
            // and throw error
            return next(new HttpNotFoundError('Instructor course exam cannot be found'));
        }
        const body = req.body;
        Args.check(Array.isArray(req.body), HttpBadRequestError);
        const items = body.map(x=>{
            return  x.testType;
        });
        const types =  courseExam.property('types');
        // first remove all test types
        await types.removeAll();
        // add types
        if (items.length) {
            await types.insert(items);
        }
        return res.json(items);

    });

    /**
     * @swagger
     *
     * /api/Instructors/Me/Exams/{courseExam}/Participants/Export:
     *  get:
     *    tags:
     *      - Instructor
     *    description: Exports a collection of students participations to the given course exam
     *    parameters:
     *      - in: path
     *        name: courseExam
     *        description: A variant which represents the id of a course exam
     *        schema:
     *          type: string
     *        required: true
     *    security:
     *      - OAuth2:
     *          - teachers
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/csv:
     *            schema:
     *              type: array
     *              items:
     *                  type: object
     *          application/vnd.openxmlformats-officedocument.spreadsheetml.sheet:
     *            schema:
     *              type: array
     *              items:
     *                  type: object
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    router.get('/me/exams/:courseExam/participants/export', bindInstructor(), async function exportCourseExamParticipants (req, res, next) {
        try {
            /**
             * get instructor object
             * @type {Instructor}
             */
            let instructor = req.params.instructor;
            // check if instructor is undefined
            if (typeof instructor === 'undefined') {
                // and throw error
                return next(new HttpForbiddenError());
            }
            /**
             * get instructor course exam
             * @type {CourseExam}
             */
            let courseExam = await instructor.getInstructorExams().where('id').equal(req.params.courseExam).select('id').getTypedItem();
            // check if course exam is undefined
            if (typeof courseExam === 'undefined') {
                // and throw error
                return next(new HttpNotFoundError('Instructor course exam cannot be found'));
            }
            // apply custom filter
            // get only $filter and $orderby query params
            let getParticipants = await req.context.model('CourseExamParticipateAction').filter({
                $filter: req.query.$filter,
                $orderby: req.query.$orderby
            });
            // set course class filter
            getParticipants.prepare().and('courseExam').equal(courseExam.id);
            /**
             * get export view
             * @type {DataModelView}
             */
            let view = getParticipants.model.getDataView('Export');
            // get student data
            let participants = await getParticipants.select('Export').getAllItems();
            // validate content type
            if (req.accepts(XlsxContentType)) {
                // return xlsx
                return res.xls(view.toLocaleArray(participants)).then(() => res.end());
            }
            // return csv
            return res.csv(view.toLocaleArray(participants)).then(() => res.end());
        }
        catch (err) {
            return next(err);
        }
    });
    /**
     * @swagger
     *
     * /api/Instructors/Me/Exams/{courseExam}/Students/Export:
     *  get:
     *    tags:
     *      - Instructor
     *    description: Exports a collection of students which are eligible to get a grade for a course exam
     *    parameters:
     *      - in: path
     *        name: courseExam
     *        description: An integer which represents the id of a course exam
     *        schema:
     *          type: integer
     *        required: true
     *    security:
     *      - OAuth2:
     *          - teachers
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/csv:
     *            schema:
     *              type: array
     *              items:
     *                  type: object
     *          application/vnd.openxmlformats-officedocument.spreadsheetml.sheet:
     *            schema:
     *              type: array
     *              items:
     *                  type: object
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    router.get('/me/exams/:courseExam/students/export', async function exportCourseExamStudents (req, res, next) {
        try {
            let getInstructor = await Instructor.getMe(req.context);
            /**
             * get instructor object
             * @type {Instructor}
             */
            let instructor = await getInstructor.select('id').getTypedItem();
            // check if instructor is undefined
            if (typeof instructor === 'undefined') {
                // and throw error
                return next(new HttpNotFoundError());
            }
            /**
             * get instructor course exam
             * @type {CourseExam}
             */
            let courseExam = await instructor.getInstructorExams().where('id').equal(req.params.courseExam).select('id').getTypedItem();
            // check if course exam is undefined
            if (typeof courseExam === 'undefined') {
                // and throw error
                return next(new HttpNotFoundError());
            }
            let getStudents = await courseExam.getStudents();
            /**
             * get export view
             * @type {DataModelView}
             */
            let view = getStudents.model.getDataView('export');
            // get student data
            let students = await getStudents.select('export').getAllItems();
            // validate content type
            if (req.accepts(XlsxContentType)) {
                // return xlsx
                return res.xls(view.toLocaleArray(students)).then(() => res.end());
            }
            // return csv
            return res.csv(view.toLocaleArray(students)).then(() => res.end());
        }
        catch (err) {
            return next(err);
        }
    });
    /**
     * @swagger
     *
     *  /api/Instructors/Me/Exams/{courseExam}/Students/Import:
     *   post:
     *    tags:
     *      - Instructor
     *    description: Uploads a *.csv or *.xlsx file which contains a collection of student grades.
     *    security:
     *     - OAuth2:
     *        - teachers
     *    parameters:
     *      - in: path
     *        name: courseExam
     *        description: An integer which represents the id of a course exam
     *        schema:
     *          type: integer
     *        required: true
     *    requestBody:
     *      content:
     *        multipart/form-data:
     *          schema:
     *            type: object
     *            properties:
     *              file:
     *                type: string
     *                format: binary
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *              schema:
     *                  $ref: '#/components/schemas/ExamDocumentUploadAction'
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    router.post('/me/exams/:courseExam/students/import', upload.single('file'),
        csvPostParser({ name: 'file' }),
        xlsPostParser({ name: 'file' }),
        async function importCourseExamStudents (req, res, next) {

            try {
                let getInstructor = await Instructor.getMe(req.context);
                /**
                 * get instructor object
                 * @type {Instructor}
                 */
                let instructor = await getInstructor.select('id').getTypedItem();
                // check if instructor is undefined
                if (typeof instructor === 'undefined') {
                    // and throw error
                    return next(new HttpNotFoundError());
                }
                /**
                 * get instructor course exam
                 * @type {CourseExam}
                 */
                let courseExam = await instructor.getInstructorExams().where('id').equal(req.params.courseExam).expand('status').getTypedItem();
                // check if course exam is undefined
                if (typeof courseExam === 'undefined') {
                    // and throw error
                    return next(new HttpNotFoundError());
                }
                if (courseExam.status.alternateName==='closed')
                {
                    return next(new HttpForbiddenError('Course exam is not accessible due to its state.'));
                }
                /**
                 * get student export view
                 * @type {DataModelView}
                 */
                let view = req.context.model('CourseExamStudentGrade').getDataView('export');
                // get students
                courseExam.grades = view.fromLocaleArray(req.body);
                // run pre check
                await courseExam.preCheck();
                if (courseExam.validationResult.success === false) {
                    // send validation failure
                    return res.status(409).json({
                        object: courseExam,
                        actionStatus: {
                            "alternateName": "FailedActionStatus"
                        }
                    });
                }
                // create course exam document action
                let documentAction = {
                    object: courseExam
                };
                // save exam document action and append request file as action attachment
                await req.context.model('ExamDocumentUploadAction').on('after.save', (event, done) => {
                    /**
                     * @type *
                     */
                    let context = event.model.context;
                    // add attachment
                    /**
                     * get private content service
                     * @type {PrivateContentService}
                     */
                    let service = context.getApplication().getService(PrivateContentService);
                    //se attachment attributes from multer file
                    let attachment = {
                        contentType: req.file.mimetype,
                        name: req.file.originalname
                    };
                    // add attachment (in unattended mode)
                    context.unattended((cb) => {
                        service.copyFrom(context, path.resolve(req.file.destination, req.file.filename), attachment, (err)=> {
                            try {
                                if (err) {
                                    return cb(err);
                                }
                                let target = event.model.convert(event.target);
                                // add attachment to document action
                                return target.property('attachments').insert(attachment).then(()=> {
                                    return cb();
                                }).catch( err => {
                                    return cb(err);
                                });
                            }
                            catch (err) {
                                return cb(err);
                            }

                        });
                    }, (err) => {
                        return done(err);
                    });
                }).silent().save(documentAction);
                // assign course exam data
                Object.assign(documentAction, {
                    object: courseExam
                });
                // return data from document action
                return res.json(documentAction);
            }
            catch (err) {
                return next(err);
            }
        });
    /**
     * @swagger
     *
     * /api/Instructors/Me/Exams/{courseExam}/Actions/:action/Complete:
     *   post:
     *    tags:
     *      - Instructor
     *    description: Uploads a *.csv or *.xlsx file which contains a collection of student grades.
     *    security:
     *      - OAuth2:
     *          - teachers
     *    parameters:
     *      - in: path
     *        name: courseExam
     *        description: An integer which represents the id of a course exam
     *        schema:
     *          type: integer
     *        required: true
     *      - in: path
     *        name: action
     *        description: An integer which represents the id of an exam document upload action
     *        schema:
     *          type: integer
     *        required: true
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/ExamDocumentUploadAction'
     *      '409':
     *        description: conflict
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/ExamDocumentUploadAction'
     *      '404':
     *        description: not found
     *      '403':
     *        description: forbidden
     *      '500':
     *        description: internal server error
     */
    router.post('/me/exams/:courseExam/actions/:action/complete', bindInstructor(), async function completeCourseExamDcoumentAction (req, res, next) {
        let documentAction;
        try {
            let instructor = req.params.instructor;
            if (typeof instructor === 'undefined') {
                return next(new HttpForbiddenError());
            }
            /**
             * get instructor course exam
             * @type {CourseExam}
             */
            let courseExam = await instructor.getInstructorExams().where('id').equal(req.params.courseExam).expand('status').getTypedItem();

            //set course exam status to completed
            courseExam.status = await req.context.model('CourseExamStatus').where('alternateName').equal('completed').getTypedItem();
            // set current user as completedByUser
            courseExam.completedByUser=instructor.user;

            if (typeof courseExam === 'undefined') {
                return next(new HttpNotFoundError('Course exam cannot be found or is inaccessible'));
            }
            // validate course exam status (other than closed)
            if (courseExam.status.alternateName === 'closed') {
                return next(new HttpConflictError('Course exam has invalid status.'));
            }
            // get document action
            documentAction = await courseExam.getDocumentActions().where('id').equal(req.params.action).getItem();
            if (typeof documentAction === 'undefined') {
                return next(new HttpNotFoundError('Document action cannot be found or is inaccessible'));
            }
            // validate action status
            if (documentAction.actionStatus.alternateName !== 'PotentialActionStatus') {
                return next(new HttpConflictError('Document action has invalid status.'));
            }
            documentAction.actionStatus = {
                "alternateName": "ActiveActionStatus"
            };
            await req.context.model('ExamDocumentUploadAction').on('before.save', (event, done) => {
                async function beforeSaveDocumentAction(event) {
                    let target = event.model.convert(event.target);
                    // get exam document action attachment
                    let attachment = await target.property('attachments').silent().getItem();
                    if (typeof attachment === 'undefined') {
                        throw new HttpNotFoundError('Document action attachment is missing.');
                    }
                    /**
                     * get private content service
                     * @type {PrivateContentService}
                     */
                    let service = event.model.context.getApplication().getService(PrivateContentService);
                    /**
                     * get attachment readable stream
                     * @type {ReadableStream}
                     */
                    let stream = await new Promise((resolve, reject) => {
                        event.model.context.unattended((cb)=> {
                            service.createReadStream(event.model.context, attachment, (err, result) => {
                                return cb(err, result);
                            });
                        }, (err, result) => {
                            if (err) {
                                return reject(err);
                            }
                            return resolve(result);
                        });
                    });
                    let body;
                    let buffer = await readStream(stream);
                    // parse text/csv
                    if (attachment.contentType === 'text/csv' || attachment.contentType === 'application/csv' ||
                        ((attachment.contentType === 'application/vnd.ms-excel' || attachment.contentType === 'application/octet-stream')
                            && (/\.(csv)$/i).test(attachment.name))) {
                        body = await new Promise((resolve, reject) => {
                            // create a request like object
                            let req = {
                                file: {
                                    buffer: buffer,
                                    mimetype: "text/csv"
                                }
                            };
                            // call csvPostHandler to parse csv
                            csvPostParser({
                                name: "file"
                            })(req, null, (err) => {
                                if (err) {
                                    reject(err);
                                }
                                // get body
                                return resolve(req.body);
                            });
                        });
                    }
                    // parse application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
                    else if (attachment.contentType === XlsxContentType || ((attachment.contentType === 'application/octet-stream')
                        && (((/\.(xlsx)$/i).test(attachment.name)) || (/\.(xls)$/i).test(attachment.name)))) {
                        body = await new Promise((resolve, reject) => {
                            // create a request like object
                            let req = {
                                file: {
                                    buffer: buffer,
                                    mimetype: XlsxContentType
                                }
                            };
                            // call csvPostHandler to parse csv
                            xlsPostParser({
                                name: "file"
                            })(req, null, (err) => {
                                if (err) {
                                    reject(err);
                                }
                                // get body
                                return resolve(req.body);
                            });
                        });
                    }
                    if (body) {
                        /**
                         * get student export view
                         * @type {DataModelView}
                         */
                        let view = req.context.model('CourseExamStudentGrade').getDataView('export');
                        // set course exam grades
                        courseExam.grades = view.fromLocaleArray(body);
                        // save course exam
                        return await new Promise((resolve, reject) => {
                            event.model.context.unattended((cb)=> {
                                courseExam.save(event.model.context, (err) => {
                                    return cb(err);
                                });
                            }, (err) => {
                                if (err) {
                                    return reject(err);
                                }
                                if (courseExam.validationResult.success === false) {
                                    // restore documentAction data
                                    documentAction.actionStatus = {
                                        "alternateName": "PotentialActionStatus"
                                    };
                                    documentAction.object = courseExam;
                                    return reject(courseExam.validationResult);
                                }
                                return resolve();
                            });
                        });
                    }
                    throw new Error('Source content type is not yet implemented');
                }
                beforeSaveDocumentAction(event).then(()=> {
                    // set course exam
                    event.target.object = courseExam;

                    //trace grades
                    let grades=courseExam.grades.filter(x=>{
                        return x.validationResult &&  x.validationResult.success === true && x.validationResult.code==='SUCC';
                    });

                    // get identifier form courseExamDocument
                    return event.model.context.model('CourseExamDocument').where('id').equal(courseExam.docId).silent().getItem().then(document=> {
                        if (document) {
                            event.target.additionalResult = document.identifier;
                        }
                        if (grades && grades.length) {
                            //prepare trace grade
                            grades.forEach(grade => {
                                delete grade.id;
                                grade.$state = 1;
                                grade.action = event.target.id;
                                grade.semester=grade.registrationSemester;
                                grade.course=courseExam.course;
                            });
                            //save student grade action trace
                            return event.model.context.model('StudentGradeActionTrace').silent().save(grades).then(traces => {
                                return done();
                            }).catch(err => {
                                return done(err);
                            });
                        }
                        return done();
                    });
                }).catch( err=> {
                    return done(err);
                });
            }).silent().save(documentAction);
            // set course exam object
            documentAction.object = courseExam;
            //get course exam result document
            if (documentAction.additionalResult) {
                documentAction.additionalResult = await req.context.model('CourseExamDocument').where('identifier').equal(documentAction.additionalResult).silent().getItem();
            }
            // return document action
            return res.json(documentAction);
        }
        catch(err) {
            if (err instanceof ValidationResult) {
                if (documentAction) {
                    return res.status(409).json(documentAction);
                }
                return res.status(409).json(err);
            }
            return next(err);
        }
    });

    /**
     * @swagger
     *
     * /api/Instructors/Me/Exams/{courseExam}/Actions/:action/Cancel:
     *   post:
     *    tags:
     *      - Instructor
     *    description: Cancels a document upload action associated with the given course exam
     *    security:
     *      - OAuth2:
     *          - teachers
     *    parameters:
     *      - in: path
     *        name: courseExam
     *        description: An integer which represents the id of a course exam
     *        schema:
     *          type: integer
     *        required: true
     *      - in: path
     *        name: action
     *        description: An integer which represents the id of an exam document upload action
     *        schema:
     *          type: integer
     *        required: true
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/ExamDocumentUploadAction'
     *      '409':
     *        description: conflict
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/ExamDocumentUploadAction'
     *      '404':
     *        description: not found
     *      '403':
     *        description: forbidden
     *      '500':
     *        description: internal server error
     */
    router.post('/me/exams/:courseExam/actions/:action/cancel', bindInstructor(), async function cancelCourseExamDocumentAction (req, res, next) {
        try {
            let instructor = req.params.instructor;
            if (typeof instructor === 'undefined') {
                return next(new HttpForbiddenError());
            }
            /**
             * get instructor course exam
             * @type {CourseExam}
             */
            let courseExam = await instructor.getInstructorExams().where('id').equal(req.params.courseExam).getTypedItem();
            if (typeof courseExam === 'undefined') {
                return next(new HttpNotFoundError('Course exam cannot be found or is inaccessible'));
            }
            // validate course exam status (other than closed)
            if (courseExam.status.alternateName === 'closed') {
                return next(new HttpConflictError('Course exam has invalid status.'));
            }
            // get document action
            let documentAction = await courseExam.getDocumentActions()
                .where('id').equal(req.params.action)
                .and('owner').notEqual(null)
                .and('owner').equal(instructor.user)
                .expand('additionalResult')
                .getItem();
            if (typeof documentAction === 'undefined') {
                return next(new HttpNotFoundError('Document action cannot be found or is inaccessible'));
            }
            if (documentAction.actionStatus.alternateName !== 'PotentialActionStatus' && documentAction.actionStatus.alternateName !== 'ActiveActionStatus') {
                return next(new HttpConflictError('Document action cannot cancelled due to its state'));
            }
            // change status
            documentAction.actionStatus = {
                alternateName: "CancelledActionStatus"
            };
            if (documentAction.additionalResult)
            {
                let courseExam= documentAction.additionalResult;
                // cancel also CourseExamDocument
                courseExam.documentStatus={
                    alternateName: "cancelled"
                };
                courseExam.documentStatusReason = req.body.notes;
                await req.context.model('CourseExamDocument').silent().save(courseExam);
            }
            await req.context.model('ExamDocumentUploadAction').silent().save(documentAction);
            // return updated document action
            return res.json(documentAction);
        }
        catch (err) {
            return next(err);
        }
    });

    router.get('/me/exams/:courseExam/:courseExamFunction', function getCourseExamFunction (req, res, next) {
        // get instructor course class
        Instructor.getMe(req.context).then(q => {
            // get instructor object
            return q.select('id').getTypedItem().then(
                /**
                 * @param {Instructor} instructor
                 */
                instructor =>  {
                    // check if instructor is undefined
                    if (typeof instructor === 'undefined') {
                        // and continue
                        return next(new HttpForbiddenError());
                    }
                    return instructor.getInstructorExams().where('id').equal(req.params.courseExam).select('id').getTypedItem().then(
                        /**
                         * @param {CourseExam} courseExam
                         */
                        courseExam=> {
                            // check if course exam is undefined
                            if (typeof courseExam === 'undefined') {
                                // and continue
                                return next(new HttpNotFoundError());
                            }
                            // call middleware
                            return getEntityFunction({
                                entityFunctionFrom: 'courseExamFunction',
                                from: 'courseExam',
                                entitySet: 'CourseExams'
                            })(req, res, next);
                        });
                }).catch(reason => {
                return next(reason);
            });
        });
    });
    /**
     * @swagger
     * /api/Instructors/Me/Theses/Students:
     *  get:
     *    tags:
     *      - Instructor
     *    description: Returns a collection of students who have been assigned one or more instructor's theses
     *    security:
     *      - OAuth2:
     *          - teachers
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              type: object
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    router.get('/me/theses/students', function getThesesStudents (req, res, next) {
        // set entity set parameters
        Object.assign(req.params, {
            entitySetFunction: "me",
            entityFunction : "thesisStudents"
        });
        // drop $select query parameter
        delete req.query.$select;
        // go to entitySetFunction middleware
        return next();
    }, getEntitySetFunction({
        entitySet: "Instructors",
    }));

    router.post('/me/exams/:courseExam/actions/:action/sign', bindInstructor(), async function signCourseExamDocumentAction (req, res, next) {
        try {
            let instructor = req.params.instructor;
            if (typeof instructor === 'undefined') {
                return next(new HttpForbiddenError());
            }
            /**
             * get instructor course exam
             * @type {CourseExam}
             */
            let courseExam = await instructor.getInstructorExams().where('id').equal(req.params.courseExam).getTypedItem();
            if (typeof courseExam === 'undefined') {
                return next(new HttpNotFoundError('Course exam cannot be found or is inaccessible'));
            }
            // validate course exam status (other than closed)
            if (courseExam.status.alternateName === 'closed') {
                return next(new HttpConflictError('Course exam has invalid status.'));
            }
            // get document action
            let documentAction = await courseExam.getDocumentActions()
                .where('id').equal(req.params.action)
                .and('owner').notEqual(null)
                .and('owner').equal(instructor.user)
                .getItem();
            if (typeof documentAction === 'undefined') {
                return next(new HttpNotFoundError('Document action cannot be found or is inaccessible'));
            }

            if (documentAction.actionStatus.alternateName !== 'ActiveActionStatus') {
                return next(new HttpConflictError('Document action cannot be signed due to its state'));
            }
            documentAction.additionalResult = req.body;
            await req.context.model('CourseExamDocument').silent().save(documentAction.additionalResult);
            // return updated document action
            return res.json(documentAction);
        }
        catch (err) {
            return next(err);
        }
    });

    router.post('/:id/messages/send', upload.single('attachment'),async function sendMessage (req, res, next) {
        try {
            // get instructor
            let instructor = await req.context.model('Instructor').where('id').equal(req.params.id).select('id').getTypedItem();
            if (typeof instructor === 'undefined') {
                // and throw error
                return next(new HttpNotFoundError('Instructor cannot be found'));
            }
            let result = await new Promise((resolve, reject) => {
                let finalResult;
                return req.context.db.executeInTransaction((cb) => {
                    let message=Object.assign({ }, req.body, {
                        instructor: instructor.id
                    });
                    if (message.action)
                    {
                        message.action=parseInt(message.action);
                    }
                    return req.context.model('InstructorMessage').save(message).then(message => {
                        // convert message to InstructorMessage
                        finalResult = req.context.model('InstructorMessage').convert(message);
                        if (req.file) {
                            // add attachment to message
                            return finalResult.addAttachment(req.file).then(result => {
                                finalResult.attachments=[];
                                finalResult.attachments.push(result);
                                return cb();
                            });
                        }
                        return cb();
                    }).catch(err => {
                        return cb(err);
                    });
                }, (err)=> {
                    if (err) {
                        return reject(err);
                    }
                    return resolve(finalResult);
                });
            });
            return res.json(result);
        }
        catch (err) {
            return next(err);
        }
    });

    router.post('/me/messages/:message/markAsRead', interactiveInstructor(), async function markAsRead (req, res, next) {
        try {
            const message = req.params.message;
            if (typeof req.instructor === 'undefined') {
                return next(new HttpForbiddenError());
            }
            /**
             * get instructorMessage
             * @type {InstructorMessage}
             */
                // check if message exists
            let instructorMessage = await req.context.model('InstructorMessage')
                    .where('id').equal(message).and('instructor').equal(req.instructor.id).getItem();
            // update dateReceived
            if (instructorMessage) {
                instructorMessage.dateReceived = new Date();
                await req.context.model('InstructorMessage').silent().save(instructorMessage);
            } else {
                return next(new HttpForbiddenError());
            }
            // return updated instructor message
            return res.json(instructorMessage);
        }
        catch (err) {
            return next(err);
        }
    });

    router.post('/me/reports/:report/print', bindInstructor(), async function printReport (req, res, next) {
        try {
            const reportTemplate = await req.context.model('ReportTemplate').where('id').equal(req.params.report).getTypedItem();
            if (typeof reportTemplate === 'undefined') {
                return next(new HttpForbiddenError('Report template cannot be found or is inaccessible'));
            }
            req.params.action = 'print'
            return postEntityAction({
                entityActionFrom: 'action',
                from: 'report',
                entitySet: 'ReportTemplates'
            })(req, res, next);
        }
        catch (err) {
            return next(err);
        }
    });

    return router;
}

export {instructorsRouter};
