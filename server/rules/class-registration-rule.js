import _ from 'lodash';
import {LangUtils} from '@themost/common/utils';
import Rule from "./../models/rule-model";
import {ValidationResult} from "../errors";
import {EdmMapping} from "@themost/data";

@EdmMapping.entityType('Rule')
/**
 * @class
 * @constructor
 * @augments Rule
 * @property {string} checkValues - A concatenated string with the courses that are going to be checked by this rule
 * @property {string} ruleOperator
 * @property {string} value1
 * @property {string} value2
 * @property {string} value3
 * @property {string} value4
 */
class ClassRegistrationRule extends Rule {
    constructor() {
        super();
    }

    /**
     * Validates an expression against the current student
     * @param {*} obj
     * @param {function(Error=,ValidationResult|*=)} done
     */
    validate(obj, done) {
        const self = this;
        const context = self.context;
        try {
            if (_.isNil(self.checkValues)) {
                return done(null, new ValidationResult(true));
            }
            if (_.isNil(obj)) {
                return done(null, new ValidationResult(true));
            }
            if (_.isNil(obj.student)) {
                return done(null, new ValidationResult(false, 'ENOSTUD', 'Student cannot be found or missing'));
            }
            /**
             * @type {DataModel}
             */
            const students = context.model('Student');
            /**
             * Represents the student associated with the given data
             * @type {Student|*}
             */
            const student = students.convert(obj.student);
            /**
             * Represents an array of prerequisites for the given data
             * @type {Array}
             */
            const values = self.checkValues.split(',');
            /**
             * Initialize the data queryable associated with the given rule
             * @type {DataQueryable|QueryExpression}
             */
            const q = context.model('StudentCourse').where('student').equal(student.getId()).and('course').in(values).prepare();
            //get operator
            const op = self.operatorOf();
            if (_.isNil(op)) {
                return done(null, new ValidationResult(false,'EOPNOIMP', 'The specified operator is not yet implemented.'));
            }
            const fnOperator = q[op];
            if (typeof fnOperator !== 'function') {
                return done(new Error('The specified operator cannot be found or is invalid'));
            }
            //apply operator expression
            q.where('grade');
            fnOperator.call(q, LangUtils.parseFloat(self.value3));
            q.silent().count(function(err, count) {
                if (err) {
                    return done(new Error('An error occurred while trying to validate prerequisites.'));
                }
                // keep result in data and pass this to validationResult
                const data= {
                    "result": count
                };
                //get minimum of prerequisites
                const minCount = LangUtils.parseInt(self.value4);
                if (minCount<=count) {
                    //success
                    done(null, new ValidationResult(true,'SUCC','The input data meets the defined prerequisites.',data));
                }
                else {
                    done(null, new ValidationResult(false,'FAIL','The input data does not meet the defined prerequisites.',data));
                }
            });
        }
        catch(e) {
            done(e);
        }
    }
}

module.exports = ClassRegistrationRule;
