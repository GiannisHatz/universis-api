import {NodeDataContext} from '@themost/node/client';
import {assert} from 'chai';
import Table from 'easy-table';

const TEST_API_SERVER = "http://localhost:5001/api/";
const TEST_BEARER_TOKEN = "2UebwTBzVXuRr9U7skxFHdCUYZU7bBMT5vMvbHR7zNkhRagr";

describe('Testing Universis Instructor Client Services', () => {
    /**
     * @type NodeDataContext
     */
    let context;
    before(done => {
        context = new NodeDataContext(TEST_API_SERVER, {
            useMediaTypeExtensions: false,
            useResponseConversion: true
        });
        context.setBearerAuthorization(TEST_BEARER_TOKEN);
        return done();
    });

    it ('should get instructors classes', async ()=> {
        // eslint-disable-next-line no-console
        console.log('INFO', 'GET /api/instructors/me/classes');
        // get instructor classes
        /**
         * @type {Array<CourseClass>}
         */
        let classes = await context.model('instructors/me/classes')
            .where('year').equal(2015)
            .and('period').equal(1)
            .expand('period')
            .getItems();
        assert.isArray(classes);
        // eslint-disable-next-line no-console
        console.log(Table.print(classes.map( x=> {
                return {
                    course: x.course,
                    title: x.title,
                    year: x.year.id,
                    period: x.period.name,
                    status: x.status
                }
        })));
    });

    it ('should get instructors classes as list', async ()=> {
        // eslint-disable-next-line no-console
        console.log('INFO', 'GET /api/instructors/me/classes');
        // get instructor classes
        /**
         * @type {ListResponse<*>}
         */
        let classes = await context.model('instructors/me/classes')
            .asQueryable()
            .expand('period')
            .orderByDescending('year')
            .thenByDescending('period')
            .take(10)
            .getList();
        assert.isArray(classes.value);
        assert.isNumber(classes.total);
        // eslint-disable-next-line no-console
        console.log('INFO', 'TOTAL', classes.total);
        assert.isNumber(classes.skip);
        // eslint-disable-next-line no-console
        console.log('INFO', 'SKIP', classes.skip);
        // eslint-disable-next-line no-console
        console.log(Table.print(classes.value.map( x=> {
            return {
                course: x.course,
                title: x.title,
                year: x.year.id,
                period: x.period.name,
                status: x.status
            }
        })));
    });

    it ('should get instructors exams', async ()=> {
        // eslint-disable-next-line no-console
        console.log('INFO', 'GET /api/instructors/me/exams');
        // get instructor classes
        /**
         * @type {Array<CourseExam>}
         */
        let exams = await context.model('instructors/me/exams')
            .where('year').equal(2015)
            .expand('examPeriod', 'course')
            .orderByDescending('examPeriod')
            .getItems();
        assert.isArray(exams);
        // eslint-disable-next-line no-console
        console.log(Table.print(exams.map( x=> {
            return {
                course: x.course.id,
                courseDisplayCode: x.course.displayCode,
                courseName: x.course.name,
                examDate: x.examDate,
                year: x.year.id,
                period: x.examPeriod.name,
                status: x.status
            }
        })));
    });

    it ('should get instructors students', async ()=> {
        // eslint-disable-next-line no-console
        console.log('INFO', 'GET /api/instructors/me/classes/students');
        // get instructor classes
        /**
         * @type {Array<StudentCourseClass>}
         */
        let students = await context.model('instructors/me/classes/students')
            .where('student/person/familyName').contains('ΑΠΟΣΤΟΛ')
            .expand('student', 'courseClass($expand=course)')
            .getItems();
        assert.isArray(students);
        // eslint-disable-next-line no-console
        console.log(Table.print(students.map( x=> {
            return {
                course: x.courseClass.course.id,
                courseDisplayCode: x.courseClass.course.displayCode,
                courseName: x.courseClass.course.name,
                student: x.student.studentIdentifier,
                familyName: x.student.familyName,
                givenName: x.student.givenName,
                inscriptionYear: x.student.inscriptionYear,
                formattedGrade: x.formattedGrade
            }
        })));
    });

    it ('should get instructor course exam students', async ()=> {
        // eslint-disable-next-line no-console
        console.log('INFO', 'GET /api/instructors/me/exams/{courseExam}/students');
        // get instructor course exams
        /**
         * @type {Array<CourseExam>}
         */
        let exams = await context.model('instructors/me/exams')
            .where('year').equal(2015)
            .expand('examPeriod', 'course')
            .orderByDescending('examPeriod')
            .getItems();
        assert.isArray(exams);

        let courseExam = exams[0].id;
        /**
         * @type {Array<CourseExamStudentGrade>}
         */
        let students = await context.model(`instructors/me/exams/${courseExam}/students`)
            .asQueryable()
            .orderBy('student/person/familyName')
            .thenBy('student/person/givenName')
            .expand('student($expand=studentStatus)')
            .getItems();
        assert.isArray(students);
        console.log(JSON.stringify(students, null, 4));

    });

    it ('should get instructor document upload actions', async ()=> {
        // eslint-disable-next-line no-console
        console.log('INFO', 'GET /api/instructors/me/exams/{courseExam}/actions');
        // get instructor course exams
        /**
         * @type {Array<CourseExam>}
         */
        let exams = await context.model('instructors/me/exams')
            .where('year').equal(2015)
            .expand('examPeriod', 'course')
            .orderByDescending('examPeriod')
            .getItems();
        assert.isArray(exams);

        let courseExam = exams[0].id;
        /**
         * @type {Array<ExamDocumentUploadAction>}
         */
        let documents = await context.model(`instructors/me/exams/${courseExam}/actions`)
            .where('actionStatus/alternateName').equal('ActiveActionStatus')
            .orderByDescending('dateCreated')
            .getItems();
        assert.isArray(documents);
        console.log(JSON.stringify(documents, null, 4));
    });

});