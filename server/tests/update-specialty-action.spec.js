import {assert} from 'chai';
import app from '../app';
import Student from '../models/student-model';
import {TestUtils} from '../utils';

describe('test update specialty actions', () => {

    /**
     * @type ExpressDataContext
     */
    let context;
    before(done => {
        /**
         * @type ExpressDataApplication
         */
        const _app = app.get('ExpressDataApplication');
        // create context
        context = _app.createContext();
        // set current student
        context.user = {
            name: 'user_20132907@example.com'
        };
        return done();
    });
    after(done => {
        if (context == null) {
            return done();
        }
        context.finalize(() => {
            return done();
        });
    });

    it('should get update specialty actions', async ()=> {
        const items = await context.model('UpdateSpecialtyAction').where('object/user/name').equal(context.user).getItems();
        assert.isArray(items);
    });

    it('should get student specialty', async ()=> {
        /**
         * @type Student
         */
        const student = await Student.getMe(context).getTypedItem();
        const specialty = await student.getSpecialty();
        assert.isObject(specialty);
    });

    it('should get student study program (with specialties)', async ()=> {
        /**
         * @type Student
         */
        const student = await Student.getMe(context).expand({
            name: 'studyProgram',
            options: {
                $expand: 'specialties'
            }
        }).getItem();
        assert.isObject(student);
        assert.isObject(student.studyProgram);
        assert.isArray(student.studyProgram.specialties);
    });

    it('should use Student.canSelectSpecialty()', async ()=> {
        /**
         * @type Student
         */
        const student = await Student.getMe(context).expand({
            name: 'studyProgram',
            options: {
                $expand: 'specialties'
            }
        }).select('id').getTypedItem();
        const canSelectSpecialty = await student.canSelectSpecialty();
        assert.isObject(canSelectSpecialty);
    });

    it('set update specialty action', async ()=> {
        await TestUtils.executeInTransaction(context, async ()=> {
            /**
             * @type Student
             */
            const student = await Student.getMe(context).expand({
                name: 'studyProgram',
                options: {
                    $expand: 'specialties'
                }
            }).getTypedItem();
            // get current specialty
            const currentSpecialty = await student.getSpecialty();
            assert.isObject(currentSpecialty);
            // can select specialty
            const canSelectSpecialty = await student.canSelectSpecialty();
            assert.equal(canSelectSpecialty.success, true);
            // set update action
            const action = {
                object: student,
                specialty: student.studyProgram.specialties[1]
            };
            await context.model('UpdateSpecialtyAction').save(action);
            assert.isDefined(action.id);
            assert.isDefined(action.currentSpecialty);
            assert.isDefined(action.specialty);
        });
    });

});
