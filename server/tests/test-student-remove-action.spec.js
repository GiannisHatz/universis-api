import {expect} from 'chai';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from '@themost/express';
import app from '../app';
import {TestUtils} from "../utils";

describe('StudentRemoveAction', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    before( (done) => {
        context = app.get(ExpressDataApplication.name).createContext();
        return done();
    });

    it('should use model', async () => {
        const model = context.model('StudentRemoveAction');
        expect(model).to.be.ok;
    });

    it('should get model attributes', async () => {
        const model = context.model('StudentRemoveAction');
        const attributes = model.attributes.filter( x => {
            if (x.primary) {
                return false;
            }
            return x.model === 'StudentRemoveAction';
        });
        expect(attributes.length).to.be.greaterThan(0);
        // the above attributes should start with removal word
        attributes.forEach( attribute => {
            expect(attribute.name).to.match(/^removal/);
        });
    });

    it('should insert a student remove action', async () => {
        await TestUtils.executeInTransaction(context, async () => {
            // get an active student
            const student = await context.model('Student')
                .where('semester').equal(2)
                .and('studentStatus/alternateName').equal('active')
                .silent().getTypedItem();
            expect(student).to.be.ok;
            // add a student remove action
            let newAction = {
                object: student.id,
                removalDate: new Date(),
                removalPeriod: 1,
                removalYear: 2018,
                removalRequest: '120/A',
                removalReason: 'A reason',
                removalDecision: '121/A',
                removalDepartment: 'Department AB',
                removalNumber: '121',
                removalComments: 'Some comments about a student removal'
            };
            // save action
            await context.model('StudentRemoveAction').silent().save(newAction);
            expect(newAction.id).to.be.ok;
            // expect state to be active
            newAction = await context.model('StudentRemoveAction').where('id').equal(newAction.id)
                .silent()
                .getTypedItem();
            expect(newAction).to.be.ok;
            expect(newAction.actionStatus.alternateName).to.be.equal('ActiveActionStatus');
        });
    });

    it('should insert a student remove action with account privileges', async () => {
        await TestUtils.executeInTransaction(context, async () => {
            context.user = null;
            // find a user
            const user = await context.model('User')
                .where('groups/name')
                .equal('Registrar')
                .and('departments/id').notEqual(null)
                .silent()
                .expand('departments')
                .getItem();
            expect(user, 'User').to.be.ok;
            expect(user.departments.length, 'Departments').to.be.ok;
            const department = user.departments[0];
            context.user = user;
            // get an active student
            let student = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .and('department').equal(department.id)
                .getTypedItem();
            expect(student, 'Student').to.be.ok;
            // add a student remove action
            let newAction = {
                object: student.id,
                actionStatus: {
                  alternateName: 'CompletedActionStatus'
                },
                removalDate: new Date(),
                removalPeriod: 1,
                removalYear: 2018,
                removalRequest: '120/A',
                removalReason: 'A reason',
                removalDecision: '121/A',
                removalDepartment: 'Department AB',
                removalNumber: '121',
                removalComments: 'Some comments about a student removal'
            };
            // save action
            await context.model('StudentRemoveAction').save(newAction);
            expect(newAction.id).to.be.ok;
            // expect state to be active
            newAction = await context.model('StudentRemoveAction').where('id').equal(newAction.id)
                .getTypedItem();
            expect(newAction).to.be.ok;
            expect(newAction.actionStatus.alternateName).to.be.equal('CompletedActionStatus');
            // get student
            student = await context.model('Student')
                .where('id').equal(newAction.object)
                .getTypedItem();
            expect(student).to.be.ok;
            expect(student.studentStatus.alternateName).to.be.equal('removed');
        });
    });

});
