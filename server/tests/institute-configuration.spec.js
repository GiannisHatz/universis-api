/* eslint no-console: off */
import {assert} from 'chai';
import app from '../app';
import Institute from '../models/institute-model';

class CancelTransactionError extends Error {
    constructor() {
        super();
    }
}

/**
 * @this {DataAdapter}
 * @param {Function} func
 */
function executeInTransactionAsync(func) {
    return new Promise((resolve, reject) => {
        this.executeInTransaction((cb) => {
            try {
                func().then(() => {
                    return cb();
                }).catch( err => {
                    return cb(err);
                });
            }
            catch (err) {
                return cb(err);
            }

        }, err => {
            if (err) {
                return reject(err);
            }
        });
    });

}

describe('institute configuration', () => {
    /**
     * @type ExpressDataContext
     */
    let context;
    before(done => {
        /**
         * @type ExpressDataApplication
         */
        const testApp = app.get('ExpressDataApplication');
        // create context
        context = testApp.createContext();
        return done();
    });
    after(done => {
        if (context == null) {
            return done();
        }
       context.finalize(()=> {
           return done();
       });
    });

    it('should load institutes', async () => {
       let institutes = await context.model('Institute').silent().getItems();
       assert.isArray(institutes);
       console.log('INFO', 'INSTITUTES', institutes);
    });

    it('should load local institute and configuration', async () => {
        let localInstitute = await context.model('Institute').where('local').equal(true).silent().getTypedItem();
        assert.isObject(localInstitute);
        assert.isObject(localInstitute.instituteConfiguration);
        console.log('INFO', 'LOCAL INSTITUTE', JSON.stringify(localInstitute, null, 2));
    });

    it('should load local institute from data object', async () => {
        let getLocalInstitute = await Institute.getLocal(context);
        let localInstitute = await getLocalInstitute.silent().getTypedItem();
        assert.isObject(localInstitute);
        assert.isObject(localInstitute.instituteConfiguration);
        console.log('INFO', 'LOCAL INSTITUTE', JSON.stringify(localInstitute, null, 2));
    });

    it('should change local institute configuration', async () => {
        /**
         * @type {Institute}
         */
        let localInstitute = await context.model('Institute').where('local').equal(true).silent().getTypedItem();
        assert.isObject(localInstitute);
        try {
            await executeInTransactionAsync.bind(context.db)(async ()=> {
                // set student register action to false
                localInstitute.instituteConfiguration.useStudentRegisterAction = false;
                // save
                await localInstitute.silent().save();
                // reload
                localInstitute = await context.model('Institute').where('local').equal(true).silent().getTypedItem();
                // validate
                assert.equal(localInstitute.instituteConfiguration.useStudentRegisterAction, false);
                // important: cancel test transaction
                throw new CancelTransactionError();
            });
        }
        catch (err) {
            if (err instanceof CancelTransactionError) {
                return;
            }
            throw err;
        }
    });

    it('should remove local institute configuration', async () => {
        /**
         * @type {Institute}
         */
        let localInstitute = await context.model('Institute').where('local').equal(true).silent().getTypedItem();
        assert.isObject(localInstitute);
        try {
            await executeInTransactionAsync.bind(context.db)(async ()=> {
                // remove institute configuration
                localInstitute.instituteConfiguration = null;
                // save
                await localInstitute.silent().save();
                // reload
                localInstitute = await context.model('Institute').where('local').equal(true).silent().getTypedItem();
                // validate
                assert.isUndefined(localInstitute.instituteConfiguration);
                // important: cancel test transaction
                throw new CancelTransactionError();
            });
        }
        catch (err) {
            if (err instanceof CancelTransactionError) {
                return;
            }
            throw err;
        }
    });

    it('should add local institute configuration', async () => {
        /**
         * @type {Institute}
         */
        let localInstitute = await context.model('Institute').where('local').equal(true).silent().getTypedItem();
        assert.isObject(localInstitute);
        try {
            await executeInTransactionAsync.bind(context.db)(async ()=> {
                // safely remove institute configuration
                localInstitute.instituteConfiguration = null;
                // save
                await localInstitute.silent().save();
                // reload
                localInstitute = await context.model('Institute').where('local').equal(true).silent().getTypedItem();
                // validate
                assert.isUndefined(localInstitute.instituteConfiguration);
                // add configuration
                localInstitute.instituteConfiguration = {
                    useStudentRegisterAction: true
                };
                // save
                await localInstitute.silent().save();
                // reload
                localInstitute = await context.model('Institute').where('local').equal(true).silent().getTypedItem();
                // validate
                assert.equal(localInstitute.instituteConfiguration.useStudentRegisterAction, true);
                // cancel test transaction
                throw new CancelTransactionError();
            });
        }
        catch (err) {
            if (err instanceof CancelTransactionError) {
                return;
            }
            throw err;
        }
    });

    it('should load local department institute', async () => {
        let department = await context.model('LocalDepartment').asQueryable().expand('organization').silent().getItem();
        assert.isObject(department);
        assert.isObject(department.organization);
        console.log('INFO', 'LOCAL DEPARTMENT', department);
    });

    it('should load local department institute configuration', async () => {
        let department = await context.model('LocalDepartment').asQueryable().expand({
            name: "organization",
            options: {
                $expand: "instituteConfiguration"
            }
        }).silent().getItem();
        assert.isObject(department);
        assert.isObject(department.organization.instituteConfiguration);
        console.log('INFO', 'LOCAL DEPARTMENT', department);
    });

});
