import {EdmMapping, DataObject} from '@themost/data';
/**
 * @class
 */
@EdmMapping.entityType('CourseClassSection')
class CourseClassSection extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

}
module.exports = CourseClassSection;
