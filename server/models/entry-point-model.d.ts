import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import User = require('./user-model');

/**
 * @class
 */
declare class EntryPoint extends DataObject {

     
     public additionalType: string;
     
     /**
      * @description An url template (RFC6570) that will be used to construct the target of the execution of the action.
      */
     public urlTemplate?: string; 
     
     /**
      * @description An application that can complete the request.
      */
     public actionApplication?: string; 
     
     /**
      * @description An application that can complete the request.
      */
     public application?: string; 
     
     /**
      * @description The high level platform(s) where the Action can be performed for the given URL. To specify a specific application or operating system instance, use actionApplication.
      */
     public actionPlatform?: string; 
     
     /**
      * @description An HTTP method that specifies the appropriate HTTP method for a request to an HTTP EntryPoint. Values are capitalized strings as used in HTTP.
      */
     public httpMethod?: string; 
     
     /**
      * @description The supported encoding type(s) for an EntryPoint request.
      */
     public encodingType?: string; 
     
     /**
      * @description The supported content type(s) for an EntryPoint response.
      */
     public contentType?: string; 
     
     /**
      * @description Μοναδικός κωδικός
      */
     public id: number; 
     
     /**
      * @description Ένα αναγνωριστικό όνομα για το στοιχείο.
      */
     public alternateName?: string; 
     
     /**
      * @description Μία σύντομη περιγραφή για το στοιχείο.
      */
     public description?: string; 
     
     /**
      * @description Η ηλεκτρονική διεύθυνση URL της εικόνας του στοιχείου.
      */
     public image?: string; 
     
     /**
      * @description Το όνομα του στοιχείου.
      */
     public name?: string; 
     
     /**
      * @description Η ηλεκτρονική διεύθυνση με περισσότερες πληροφορίες για το στοιχείο. Π.χ. τη διεύθυνση URL της σελίδας Wikipedia ή του επίσημου ιστότοπου.
      */
     public url?: string; 
     
     /**
      * @description Η ημερομηνία δημιουργίας του στοιχείου
      */
     public dateCreated?: Date; 
     
     /**
      * @description Η ημερομηνία τροποποίησης του στοιχείου
      */
     public dateModified?: Date; 
     
     /**
      * @description Ο χρήστης που δημιούργησε το στοιχείο.
      */
     public createdBy?: User|any; 
     
     /**
      * @description Ο χρήστης που τροποποίησε το στοιχείο.
      */
     public modifiedBy?: User|any; 

}

export = EntryPoint;