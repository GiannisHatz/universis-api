import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {string} additionalType
 * @property {string} alternateName
 * @property {string} name
 * @property {number} identifier
 * @property {number} id
 * @property {string} description
 * @property {string} image
 * @property {string} url
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {User|any} createdBy
 * @property {User|any} modifiedBy
 */
@EdmMapping.entityType('ActionStatusType')
class ActionStatusType extends DataObject {

    static get PotentialActionStatus() {
        return 'PotentialActionStatus';
    }

    static get FailedActionStatus() {
        return 'FailedActionStatus';
    }

    static get CompletedActionStatus() {
        return 'CompletedActionStatus';
    }

    static get ActiveActionStatus() {
        return 'ActiveActionStatus';
    }

    static get CancelledActionStatus() {
        return 'CancelledActionStatus';
    }

    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = ActionStatusType;
