import {EdmMapping, EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
import {DataError, TraceUtils} from "@themost/common";
import ActionStatusType from "./action-status-type-model";
let UploadAction = require('./upload-action-model');

/**
 * @this DataAdapter
 * @param func
 * @returns {Promise<unknown>}
 */
function executeInTransactionAsync(func) {
    return new Promise((resolve, reject) => {
        return this.executeInTransaction((callback) => {
            return func.call(this).then(res => {
                return callback(null, res);
            }).catch(err => {
                return callback(err);
            });
        }, (err, res) => {
            if (err) {
                return reject(err);
            }
            return resolve(res);
        });
    });
}

/**
 * @class

 * @property {ExamDocumentUploadAction|any} additionalResult
 * @property {Array<StudentGradeActionTrace|any>} grades
 * @property {number} id
 * @augments {DataObject}
 */
@EdmMapping.entityType('ExamDocumentUploadAction')
class ExamDocumentUploadAction extends UploadAction {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * @returns {ExamDocumentUploadAction}
     */
    @EdmMapping.action('approve', 'ExamDocumentUploadAction')
    approve() {
        return this.changeActionStatus(ActionStatusType.CompletedActionStatus);

    }

    /**
     * @returns {ExamDocumentUploadAction}
     */
    @EdmMapping.action('reject', 'ExamDocumentUploadAction')
    reject() {
        return this.changeActionStatus(ActionStatusType.FailedActionStatus);

    }


    async changeActionStatus(status) {
        try {
            // check status
            // get action status
            const actionStatus = await this.property('actionStatus').getItem();
            // if action status is other that active
            if (actionStatus.alternateName !== ActionStatusType.ActiveActionStatus) {
                throw new DataError('ERR_INVALID_DATA', this.context.__('Cannot approve or reject exam action due to its state'));
            }
            // TODO: validateSignature and process xml grades

            /**
             * get ExamDocumentUploadAction
             * @type {ExamDocumentUploadAction}
             */
            let uploadAction = await this.context.model('ExamDocumentUploadAction')
                .asQueryable()
                .where('id').equal(this.getId())
                .expand('object', 'grades')
                .silent().getItem();
            //check if there is a previous active action
            /**
             * get CourseExam
             * @type {CourseExam}
             */
            const courseExam = await this.context.model('CourseExam').where('id').equal(uploadAction.object.id).expand('status').getTypedItem();
            const documentActions = await courseExam.getDocumentActions().where('additionalResult').notEqual(null).getItems();

            if (documentActions.length > 1) {
                const findPreviousActive = documentActions.find(x => {
                    return x.actionStatus.alternateName === 'ActiveActionStatus' && x.id < this.getId();
                });
                if (findPreviousActive) {
                    throw new DataError('ERR_INVALID_DATA', this.context.__('You must first approve or reject previous active action'));
                }
            }

            await executeInTransactionAsync.bind(this.context.db)(async () => {
                if (uploadAction.grades.length > 0) {
                    // save student grades
                    const grades = [];
                    const examGrades = await this.context.model('StudentGrade').where('courseExam').equal(courseExam.id).getAllItems();

                    for (let i = 0; i < uploadAction.grades.length; i++) {
                        const newGrade = uploadAction.grades[i];
                        // check if grade exists
                        /**
                         * @type {StudentGrade}
                         */
                        let examGrade = examGrades.find(x => {
                            return x.student === newGrade.student;
                        });
                        if (examGrade) {
                            if (examGrade.status.alternateName !== 'normal' && status === ActionStatusType.FailedActionStatus) {   // remove grades only if status is pending
                                await this.context.model('StudentGrade').remove(examGrade);
                            }
                            if (status === ActionStatusType.CompletedActionStatus) {
                                examGrade.examGrade = newGrade.examGrade;
                                examGrade.grade1 = newGrade.examGrade;
                                examGrade.status = {"alternateName": "normal"};
                                grades.push(examGrade);
                            }
                        } else {
                            // add grade
                            if (status === ActionStatusType.CompletedActionStatus) {
                                examGrade = {
                                    student: newGrade.student,
                                    courseExam: newGrade.courseExam,
                                    examGrade: newGrade.examGrade,
                                    grade1: newGrade.examGrade,
                                    status: {"alternateName": "normal"}
                                };
                                grades.push(examGrade);
                            }
                        }
                    }
                    const failedGrades = [];
                    if (grades.length > 0) {
                        //save grades
                        for (let grade of grades) {
                            try {
                                await this.context.model('StudentGrade').save(grade);
                            } catch (err) {
                                failedGrades.push({"grade": grade, "error": err});
                                // delete invalid grade
                                await this.context.model('StudentGrade').remove(grade);
                            }
                        }
                    }
                    // change also gradeStatus of trace
                    uploadAction.grades = uploadAction.grades.map(x => {
                        if (status === ActionStatusType.CompletedActionStatus) {
                            const failed = failedGrades.find(y => {
                                return y.grade.student === x.student;
                            });
                            x.status = failed ? {"alternateName": "failed"} : {"alternateName": "normal"};
                            x.description = failed ? failed.error.message : null;
                        } else {
                            x.status = {"alternateName": "failed"};
                            x.description = this.context.__('Exam action is rejected');
                        }
                        return x;
                    });
                    await this.context.model('StudentGradeActionTrace').silent().save(uploadAction.grades);
                    uploadAction.failed = failedGrades;
                }
                // add acceptAction as result
                const acceptAction = await this.context.model('AcceptAction').save({
                    "actionStatus": {"alternateName": status},
                    "description": status === ActionStatusType.CompletedActionStatus ? this.context.__('Accept exam grades') : this.context.__('Reject exam grades'),
                    "object": uploadAction.id,
                    "additionalType": "ExamDocumentUploadAction"
                });
                uploadAction.actionResult = acceptAction;
                //change action status
                uploadAction.actionStatus = {"alternateName": status};

                await this.context.model('ExamDocumentUploadAction').silent().save(uploadAction);
            });
            return uploadAction;
        } catch (e) {
            throw e;

        }
    }
}

module.exports = ExamDocumentUploadAction;
