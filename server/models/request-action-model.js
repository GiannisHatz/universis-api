import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
let InteractAction = require('./interact-action-model');
/**
 * @class
 
 * @property {number} id
 * @augments {DataObject}
 */
@EdmMapping.entityType('RequestAction')
class RequestAction extends InteractAction {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = RequestAction;