import {EdmMapping, EdmType, DataObject, DataPermissionEventListener, DataObjectState} from '@themost/data';
import {promisify} from "es6-promisify";
const Rule = require('./rule-model');
/**
 * @class
 
 * @property {number} id
 * @property {Department|any} department
 * @property {StudyLevel|any} studyLevel
 * @property {boolean} isActive
 * @property {string} name
 * @property {string} abbreviation
 * @property {string} printName
 * @property {string} degreeDescription
 * @property {GradeScale|any} gradeScale
 * @property {number} decimalDigits
 * @property {boolean} hasFees
 * @property {number} semesters
 * @property {Semester|any} specialtySelectionSemester
 * @property {Array<StudyProgramSpecialty|any>} specialties
 */
@EdmMapping.entityType('StudyProgram')
class StudyProgram extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * Returns a collection of rules which are going to be check while validating that a student is eligible for graduation
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('GraduationRules', EdmType.CollectionOf('Rule'))
    async getGraduationRules() {
        // return rules of type ProgramCourseRegistrationRule
        return Rule.expand(this.context, this.getId(),
            'Program', 'GraduateRule');
    }

    /**
     * Updates program course registration rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
    @EdmMapping.action('GraduationRules', EdmType.CollectionOf('Rule'))
    async setGraduationRules(items) {
        const finalItems = [];
        for(let item of items) {
            // convert item
            const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
            // set target id
            converted.target = this.getId().toString();
            // set target type
            converted.targetType = 'Program';
            // set additional type
            converted.additionalType = 'GraduateRule';
            finalItems.push(converted);
        }
        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.getModel(), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        // save items
        await this.context.model(Rule).silent().save(finalItems);
        // and return collection of rules
        return this.getGraduationRules();
    }

    /**
     * Returns a collection of rules which are going to be check while validating
     * that a student is eligible to enroll for this program
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('InscriptionRules', EdmType.CollectionOf('Rule'))
    async getProgramInscriptionRules() {
        // return rules of type ProgramCourseRegistrationRule
        return Rule.expand(this.context, this.getId(),
            'Program', 'ProgramInscriptionRule');
    }

    /**
     * Updates program enrollment rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
    @EdmMapping.action('InscriptionRules', EdmType.CollectionOf('Rule'))
    async setProgramInscriptionRule(items) {
        const finalItems = [];
        for(let item of items) {
            // convert item
            const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
            // set target id
            converted.target = this.getId().toString();
            // set target type
            converted.targetType = 'Program';
            // set additional type
            converted.additionalType = 'ProgramInscriptionRule';
            finalItems.push(converted);
        }
        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.getModel(), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        // save items
        await this.context.model(Rule).silent().save(finalItems);
        // and return collection of rules
        return this.getProgramInscriptionRules();
    }

    /**
     * Returns a collection of rules which are going to be checked while validating a student internship
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('InternshipRules', EdmType.CollectionOf('Rule'))
    async getInternshipRules() {
        // return rules of type ProgramCourseRegistrationRule
        return Rule.expand(this.context, this.getId(),
            'Program', 'InternshipRule');
    }

    /**
     * Updates student internship rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
    @EdmMapping.action('InternshipRules', EdmType.CollectionOf('Rule'))
    async setInternshipRules(items) {
        const finalItems = [];
        for(let item of items) {
            // convert item
            const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
            // set target id
            converted.target = this.getId().toString();
            // set target type
            converted.targetType = 'Program';
            // set additional type
            converted.additionalType = 'InternshipRule';
            // add item
            finalItems.push(converted);
        }
        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.getModel(), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        // save items
        await this.context.model(Rule).silent().save(finalItems);
        // and return new collection
        return this.getInternshipRules();
    }

    /**
     * Returns a collection of rules which are going to be checked while validating a student thesis
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('ThesisRules', EdmType.CollectionOf('Rule'))
    async getThesisRules() {
        // return rules of type ProgramCourseRegistrationRule
        return Rule.expand(this.context, this.getId(),
            'Program', 'ThesisRule');
    }

    /**
     * Updates student internship rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
    @EdmMapping.action('ThesisRules', EdmType.CollectionOf('Rule'))
    async setThesisRules(items) {
        const finalItems = [];
        for(let item of items) {
            // convert item
            const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
            // set target id
            converted.target = this.getId().toString();
            // set target type
            converted.targetType = 'Program';
            // set additional type
            converted.additionalType = 'ThesisRule';
            // add item
            finalItems.push(converted);
        }
        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.getModel(), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        // save items
        await this.context.model(Rule).silent().save(finalItems);
        // and return new collection
        return this.getThesisRules();
    }

}
module.exports = StudyProgram;
