import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {number} id
 * @property {Department|any} department
 * @property {string} name
 * @property {Date} startDate
 * @property {AcademicYear|any} startYear
 * @property {AcademicPeriod|any} startPeriod
 * @property {Date} endDate
 * @property {Instructor|any} instructor
 * @property {number} coefficient
 * @property {string} notes
 * @property {number} subject
 * @property {string} otherInstructors
 * @property {ThesisType|any} type
 * @property {ThesisStatus|any} status
 * @property {number} grade
 * @property {number} isPassed
 * @property {string} formattedGrade
 * @property {number} units
 * @property {GradeScale|any} gradeScale
 * @property {number} ects
 * @property {Date} examDate
 * @property {Date} dateModified
 * @property {Array<StudentThesis|any>} students
 * @augments {DataObject}
 */
@EdmMapping.entityType('Thesis')
class Thesis extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = Thesis;