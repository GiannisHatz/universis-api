import {EdmMapping,EdmType} from '@themost/data/odata';
import _ from 'lodash';
import {DataObject} from "@themost/data/data-object";
import {ValidationResult} from "../errors";
import async from 'async';

@EdmMapping.entityType("StudentRequestConfiguration")
/**
 * @class
 * @augments DataObject
 */
class StudentRequestConfiguration extends DataObject {

    /**
     * @constructor
     */
    constructor() {
        super();
    }

    validate(callback) {
        const self = this, context = self.context;
        try {
            let validationResults;
            // get request rules
            return context.model('RequestRule').where('target').equal(self.id).and('additionalType').equal(['RequestRule'])
                .and('targetType').equal('StudentRequestConfiguration').getAllItems().then(rulesData => {

                    if (rulesData.length === 0) {
                        return callback(null, new ValidationResult(true, 'SUCC', context.__('Request configuration does not have any rules.')));
                    }
                    async.eachSeries(rulesData, function (item, cb) {
                        const ruleModel = context.model(item.refersTo + 'Rule');
                        if (_.isNil(ruleModel)) {
                            return callback(new Error(context.__('Request configuration rule type cannot be found.')));
                        }
                        const rule = ruleModel.convert(item);
                        rule.validate(self, function (err, result) {
                            if (err) {
                                return callback(err);
                            }
                            /**
                             * @type {ValidationResult[]}
                             */
                            validationResults = validationResults || [];
                            validationResults.push(result);
                            cb();
                        });
                    }, function (err) {
                        if (err) {
                            return callback(err);
                        }

                        const fnValidateExpression = function (x) {
                            try {
                                let expr = x['ruleExpression'];
                                if (_.isEmpty(expr)) {
                                    return false;
                                }
                                expr = expr.replace(/\[%(\d+)]/g, function () {
                                    if (arguments.length === 0) return;
                                    const id = parseInt(arguments[1]);
                                    const v = validationResults.find(function (y) {
                                        return y.id === id;
                                    });
                                    if (v) {
                                        return v.success.toString();
                                    }
                                    return 'false';
                                });
                                expr = expr.replace(/\bAND\b/ig, ' && ').replace(/\bOR\b/ig, ' || ').replace(/\bNOT\b/ig, ' !');
                                return eval(expr);
                            } catch (e) {
                                return e;
                            }
                        };

                        const fnTitleExpression = function (x) {
                            try {
                                let expr = x['ruleExpression'];
                                if (_.isEmpty(expr)) {
                                    return false;
                                }
                                expr = expr.replace(/\[%(\d+)]/g, function () {
                                    if (arguments.length === 0) return;
                                    const id = parseInt(arguments[1]);
                                    const v = validationResults.find(function (y) {
                                        return y.id === id;
                                    });
                                    if (v) {
                                        return '(' + v.message.toString() + ')';
                                    }
                                    return 'Unknown Rule';
                                });
                                expr = expr.replace(/\bAND\b/ig, context.__(' AND ')).replace(/\bOR\b/ig, context.__(' OR ')).replace(/\bNOT\b/ig, context.__(' NOT '));
                                return expr;
                            } catch (e) {
                                return e;
                            }
                        };

                        const finalValidationResults = [];

                        let res, title;

                        //apply default expression
                        const expr = rulesData.find(function (x) {
                            return !_.isEmpty(x['ruleExpression']);
                        });
                        let finalResult;
                        if (expr) {
                            res = fnValidateExpression(expr);
                            title = fnTitleExpression(expr);
                            finalResult = new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
                            finalResult.type = 'GraduationRule';
                            expr.ruleExpression = expr.ruleExpression.replace(/\bAND\b/ig, ' && ').replace(/\bOR\b/ig, ' || ').replace(/\bNOT\b/ig, ' !');
                            expr.ruleExpression = expr.ruleExpression.replace(/\[%(\d+)]/g, function () {
                                if (arguments.length === 0) return;
                                return parseInt(arguments[1]);
                            });
                            finalResult.expression = expr.ruleExpression;
                        } else {
                            //get expression (for this rule type)
                            const ruleExp1 = rulesData.map(function (x) {
                                return '[%' + x.id + ']';
                            }).join(' AND ');
                            res = fnValidateExpression({ruleExpression: ruleExp1});
                            title = fnTitleExpression({ruleExpression: ruleExp1});
                            finalResult = new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
                            finalResult.type = 'RequestRule';
                            finalResult.expression = null;
                        }
                        return callback(null, finalResult);
                    });
                });
        } catch (e) {
           return callback(e);
        }
    }
}

module.exports = StudentRequestConfiguration;
