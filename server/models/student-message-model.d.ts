import {EdmMapping,EdmType} from '@themost/data/odata';
import Message = require('./message-model');
import Student = require('./student-model');

/**
 * @class
 */
declare class StudentMessage extends Message {

     
     /**
      * @description Student
      */
     public student: Student|any; 
     
     /**
      * @description Μοναδικός κωδικός
      */
     public id: number; 

}

export = StudentMessage;