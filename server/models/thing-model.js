import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {number} id
 * @property {string} additionalType
 * @property {string} alternateName
 * @property {string} description
 * @property {string} image
 * @property {string} name
 * @property {string} url
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {User} createdBy
 * @property {User} modifiedBy
 * @augments {DataObject}
 */
@EdmMapping.entityType('Thing')
class Thing extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = Thing;