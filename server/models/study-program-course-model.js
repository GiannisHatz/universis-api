import {EdmMapping, EdmType, DataObject} from '@themost/data';
const Rule = require('./rule-model');
/**
 * @class
 
 * @property {number} id
 * @property {Course|any} course
 * @property {Course|any} studyProgram
 * @property {ProgramGroup|any} programGroup
 * @property {number} programGroupFactor
 * @property {number} sortIndex
 * @property {string} identifier
 * @property {Date} dateModified
 */
@EdmMapping.entityType('StudyProgramCourse')
class StudyProgramCourse extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * Returns a collection or rules which are going to be validated during course registration
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('ProgramCourseRegistrationRules', EdmType.CollectionOf('Rule'))
    async getProgramCourseRegistrationRules() {
        // return rules of type ProgramCourseRegistrationRule
        return Rule.expandRules(this.context, this.getId(),
            'ProgramCourse', 'ProgramCourseRegistrationRule');
    }

}
module.exports = StudyProgramCourse;
