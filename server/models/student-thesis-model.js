import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {*} id
 * @property {Thesis|any} thesis
 * @property {Student|any} student
 * @property {Date} dateCompleted
 * @property {number} grade
 * @property {number} isPassed
 * @property {string} formattedGrade
 * @property {Date} dateModified
 * @property {Array<StudentThesisResult|any>} results
 * @augments {DataObject}
 */
@EdmMapping.entityType('StudentThesis')
class StudentThesis extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = StudentThesis;