import { FunctionContext } from '@themost/data/functions';
import {parsers} from '@themost/data/types';
import _ from 'lodash';
import moment from "moment";
/**
 * Returns the current academic year based based on the department associated with the current context (e.g. the department of a student etc).
 */
_.assign(FunctionContext.prototype, {
    /**
     * @this FunctionContext
     * @returns {Promise<any>}
     */
    student() {
        const self = this;
        return new Promise((resolve, reject) => {
            self.model.resolveMethod('student', [], function (err, result) {
                if (err) {
                    return reject(err);
                }
                return resolve(result);
            });
        });
    },
    /**
     * Returns the current academic year based on the department associated with the current context (e.g. the department of a student etc).
     * @returns {Promise<any>}
     */
    currentYear() {
        let self = this;
        return new Promise((resolve, reject) => {
            self.model.resolveMethod('currentYear', [], function (err, result) {
                if (err) {
                    return reject(err);
                }
                resolve(result);
            });
        });
    },
    /**
     * Returns the current academic period based on the department associated with the current context (e.g. the department of a student etc).
     * @returns {Promise}
     */
    currentPeriod() {
        let self = this;
        return new Promise((resolve, reject) => {
            self.model.resolveMethod('currentPeriod', [], function (err, result) {
                if (err) {
                    return reject(err);
                }
                resolve(result);
            });
        });
    },
    /**
     * Returns an instructor identifier if the current context interactive user is an instructor
     * @returns {Promise<any>}
     */
    instructor() {
        let self = this;
        return new Promise((resolve, reject) => {
            self.model.resolveMethod('instructor', [], function (err, result) {
                if (err) {
                    return reject(err);
                }
                resolve(result);
            });
        });
    },
    /**
     * Returns the department identifier associated with the current context (e.g. the department of a student etc).
     * @returns {Promise}
     */
    department() {
        let self = this;
        return new Promise((resolve, reject) => {
            self.model.resolveMethod('department', [], function (err, result) {
                if (err) {
                    return reject(err);
                }
                resolve(result);
            });
        });
    },

    /**
     * Returns strict boolean (-1,0)  (e.g. the isActive property of StudyProgram is boolean but should be stored as -1,0).
     * @returns {Promise}
     */
    convertStrictBoolean(target, property) {
        let self = this;
        return new Promise((resolve, reject) => {
            if (target.hasOwnProperty(property)) {
                return resolve(-Math.abs(parsers.parseInteger(target[property])));
            }
            const primaryKey = self.model.getPrimaryKey();
            self.model.context.model(self.model.name)
                .where(primaryKey).equal(target[primaryKey]).select(property).value().then(function (value) {
                return resolve(-Math.abs(parsers.parseInteger(value)));
            }).catch(function (err) {
                return reject(err);
            });
        });
    },
    /**
     * Returns id for new courseClass based on courseClassFormat  (e.g. {course-year-period}, {00000}).
     * @returns {Promise}
     */
    newCourseClass(target) {
        const self = this;
        return new Promise((resolve, reject) => {
            const context = self.model.context;
            context.db.selectIdentity('CourseClassSequence', 'id', (err, res) => {
                if (err) {
                   return reject(err);
                }
                if (target['course'] && target['year'] && target['period']) {
                    let identifier = null;
                    let courseClassFormat = 'I';
                    // get courseClassFormat from settings
                    let universisConfiguration = context.getApplication().getConfiguration().getSourceAt('settings/universis');
                    if (typeof universisConfiguration !== 'undefined') {
                        courseClassFormat = universisConfiguration.courseClassFormat || 'I';
                    }
                    const course = _.isObject(target['course']) ? target['course'].id : target['course'];
                    const year = _.isObject(target['year']) ? target['year'].id : target['year'];
                    const period = _.isObject(target['period']) ? target['period'].id : target['period'];
                    // calculate new id
                    identifier = courseClassFormat;
                    let format = courseClassFormat.split(';');
                    for (let i = 0; i < format.length; i++) {
                        let formatElement = format[i];
                        if (formatElement.startsWith('I')) {
                            // index with leading zeros
                            identifier = identifier.replace(formatElement, this.zeroPad((res + 1), formatElement.length - 1));
                        }
                        if (formatElement.startsWith('T')) {
                            // text
                            identifier = identifier.replace(formatElement, formatElement.substr(1));
                        }
                        if (formatElement.startsWith('C')) {
                            // text
                            identifier = identifier.replace(formatElement, course);
                        }
                        if (formatElement.startsWith('Y')) {
                            // year
                            let today = new Date();
                            today.setFullYear(year, 1, 1, 0);
                            identifier = identifier.replace(formatElement, moment(today).format(formatElement));
                        }
                        if (formatElement.startsWith('P')) {
                            // period
                            identifier = identifier = identifier.replace(formatElement, period);
                        }
                    }
                    identifier = identifier.replace(/;/g, '');
                    return resolve(identifier);
                } else {
                    return resolve();
                }
            });
        })
    },
    zeroPad(num, places) {
        let zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }
});
